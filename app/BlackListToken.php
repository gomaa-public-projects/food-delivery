<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlackListToken extends Model
{
    protected $fillable = ['token'];
}
