<?php

namespace App\LocalProviders;

use App\AuthUser;
use Illuminate\Contracts\Auth\UserProvider;
use App\LocalProviders\Traits\UserProviderTrait;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\LightHouseCustomException;
use Illuminate\Support\Facades\Log;
use Twilio\Rest\Client;

class PhoneUserProvider implements UserProvider
{
    use UserProviderTrait;

    public function validateCredentials(?Authenticatable $user, array $credentials)
    {
        $user = $this->retrieveByCredentials($credentials);
        if(!isset($credentials['phone']) || !isset($credentials['password']) ||$user->phone != $credentials['phone'] || !Hash::check($credentials['password'],$user->password)){
            throw new LightHouseCustomException(403, __("wrong_creds"));
        }
        
    }

    public function retrieveByCredentials(array $credentials)
    {
        return AuthUser::where('phone',$credentials['phone'])->first();
    }

    public function validateOtp(array $credentials)
    {

        Log::channel('authentication')->info("Current Phone is: ".$credentials['phone']);
        $twilio = new Client(config('sms.ACCOUNT_SID'), config('sms.AUTH_TOKEN'));
        $verification_check=[];
        try{
            $verification_check = $twilio->verify->v2->services(config('sms.SERVICE_SID'))
                ->verificationChecks
                ->create(
                    $credentials['code'], // code
                    ["to" => $credentials['phone']]
                );
        }catch(Exception $e){
            Log::channel('authentication')->info("OTP Error: ".$e->getMessage());
            return false;
        }
           
        return $verification_check->status === 'approved';
    }
}
