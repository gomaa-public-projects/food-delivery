<?php

namespace App\LocalProviders\Traits;

use App\AuthUser;
use \Firebase\JWT\JWT;
use Akwad\VoyagerExtension\Models\Role;
use Illuminate\Contracts\Auth\Authenticatable;

trait UserProviderTrait
{
    public function __construct()
    {

    }
    public function retrieveById($identifier)
    {
        $role = AuthUser::where('id',$identifier)->firstOrFail()->role;
        $model = Role::where('name', $role)->firstOrFail()->model;
        return $model::where('auth_user_id', $identifier)->firstOrFail();
    }

    public function retrieveByToken($identifier, $token)
    {
        return NULL;
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        return NULL;
    }

    public function getModel(){
        return NULL;
    }
}
