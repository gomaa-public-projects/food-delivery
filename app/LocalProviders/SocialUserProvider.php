<?php

namespace App\LocalProviders;

use App\AuthUser;
use Illuminate\Contracts\Auth\UserProvider;
use App\LocalProviders\Traits\UserProviderTrait;
use Illuminate\Contracts\Auth\Authenticatable;

class SocialUserProvider implements UserProvider
{
    use UserProviderTrait;

    public function validateCredentials(?Authenticatable $user, array $credentials)
    {
        return true;
    }

    public function retrieveByCredentials(array $credentials)
    {
        return AuthUser::where('token',$credentials['token'])->first();
    }
}
