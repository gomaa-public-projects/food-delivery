<?php

namespace App\Observers;

use App\Branch;
use Carbon\Carbon;
use App\BranchAddress;
class BranchObserver 
{

    public function creating(Branch $branch)
    {

    }

    public function created(Branch $branch)
    {
        BranchAddress::create([
            'branch_id' => $branch->id,
            'longitude' => $branch->longitude,
            'latitude' => $branch->latitude,
            'description' => $branch->description
        ]);
    }

    public function updating(Branch $branch)
    {
        $changes = $branch->getDirty();

        if((isset($changes['longitude']) ||  isset($changes['latitude'])) && $branch->branchAddress != null){
            $branch->branchAddress->longitude = $branch->longitude;
            $branch->branchAddress->latitude = $branch->latitude;
            $branch->branchAddress->save();
        }
        if(isset($changes['description']) && $branch->branchAddress != null){
            $branch->branchAddress->description = $branch->description;
            $branch->branchAddress->save();
        }
    }

    public function updated(Branch $branch)
    {
        if($branch->delete){
            if($branch->branchAddress){
                $branch->branchAddress->delete();
            }
            $branch->delete();
        }
    }

    
    public function deleted(Branch $branch)
    {
        //
    }

    
    public function restored(Branch $branch)
    {
        //
    }

    
    public function forceDeleted(Branch $branch)
    {
        //
    }

}
