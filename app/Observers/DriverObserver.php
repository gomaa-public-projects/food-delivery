<?php

namespace App\Observers;

use App\Driver;
use Carbon\Carbon;

class DriverObserver 
{

    public function creating(Driver $driver)
    {

    }

    public function created(Driver $driver)
    {

    }

    public function updating(Driver $driver)
    {

    }

    public function updated(Driver $driver)
    {
        if($driver->delete){
            $driver->delete();
        }
    }

    
    public function deleted(Driver $driver)
    {
        //
    }

    
    public function restored(Driver $driver)
    {
        //
    }

    
    public function forceDeleted(Driver $driver)
    {
        //
    }

}
