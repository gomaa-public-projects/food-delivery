<?php

namespace App\Observers;

use App\Driver;
use App\Order;
use App\Branch;
use Carbon\Carbon;
use App\AppSetting;
use App\RejectedInvitation;
use Illuminate\Support\Facades\Log;
use App\Events\UpdateOrderStatusEvent;
use App\Events\DeleteOrderNodeEvent;
use App\Exceptions\LightHouseCustomException;
use App\MobileNotifications\DriverNewRequest;
use App\MobileNotifications\DriverOrderIsReady;
use App\MobileNotifications\CustomerOrderDeliverd;
use App\MobileNotifications\CustomerOrderIsReady;
use App\MobileNotifications\CustomerOrderOnHisWay;
use App\MobileNotifications\CustomerOrderRejected;
use App\Http\Controllers\PaymentController;

class OrderObserver extends PaymentController
{

    public function creating(Order $order)
    {
        // if(!$order->customer->has_payment_info){
        //     throw new LightHouseCustomException(403, "no payment info found");
        // }
        $distances = array();
        $branches = Branch::all();
        foreach($branches as $branch){
            if(!$branch->branchAddress){
                continue;
            }
            $distances[$branch->id] = resolve('distance')->calculate($order->customer->address->latitude,$order->customer->address->longitude,$branch->branchAddress->latitude,$branch->branchAddress->longitude);
        }
        asort($distances);
        $branch_id = array_key_first($distances);
        Log::channel('observers')->info("OrderObserver (creating): Branch Id:".$branch_id);
        $allowed_distance = floatval(AppSetting::where('key','customer_branch_distance')->first()->value);

        if(!$branch_id || $allowed_distance < $distances[$branch_id]){
            Log::channel('observers')->info("OrderObserver (creating): can't find a branch near you:");
            throw new LightHouseCustomException(403, "can't find a branch near you");
        }
        $order->branch_id = $branch_id;
        Log::channel('observers')->info("OrderObserver (creating): Branch Distance:".$distances[$branch_id]);
        Log::channel('observers')->info("OrderObserver (creating): all Branches Distances:".json_encode($distances));
        $drivers_distances = array();

        $drivers = Driver::where('branch_id',$branch_id)->where('available',true)->doesntHave('orders','and', function($q){
            $q->whereNotIn('status', ['delivered','rejected']);
        })->get();
        foreach($drivers as $driver){
            $location = $driver->location;
            if($location->latitude  == 0|| $location->longitude == 0) continue;
            $drivers_distances[$driver->id] = resolve('distance')->calculate($order->branch->branchAddress->latitude,$order->branch->branchAddress->longitude, $location->latitude, $location->longitude);
        }

        asort($drivers_distances);
        $driver_id = array_key_first($drivers_distances);

        $allowed_distance = floatval(AppSetting::where('key','driver_branch_distance')->first()->value);

        if($driver_id){
            Log::channel('observers')->info("OrderObserver (creating): Driver Distance:".$drivers_distances[$driver_id]);  
        }
        Log::channel('observers')->info("OrderObserver (creating): all Driver Distances:".json_encode($drivers_distances));

        Log::channel('observers')->info("OrderObserver (creating): Driver ID:".$driver_id);
        if(!$driver_id || $allowed_distance < $drivers_distances[$driver_id]){
            Log::channel('observers')->info("OrderObserver (creating): can't find a driver near you");
            throw new LightHouseCustomException(403, "can't find a driver near you");
        }
        $order->status = "waiting_driver";
        // $order->driver_id = $driver_id;

        // $driver_timeout_minutes = intval(AppSetting::where('key','driver_timeout_minutes')->first()->value);
        // $order->invitation_time = Carbon::now()->addMinutes($driver_timeout_minutes);
    }

    public function created(Order $order)
    {
        $open_orders = Order::where('customer_id',$order->customer_id)->where('id','!=',$order->id)->whereIn('status',["pending", "waiting_driver"])->whereNull("payment_status")->get();
        foreach($open_orders as $open_order){
            $open_order->status = "archived";
            $open_order->save();
        }
        
    }

    public function updating(Order $order)
    {
        $changes = $order->getDirty();
        if(isset($changes['status']) || isset($changes['payment_method']) && $order->status == 'closed'){
            event(new UpdateOrderStatusEvent($order));
        }
        if(isset($changes['status']) && $order->status == 'ready'){
            $order->driver->authUser->notify(new DriverOrderIsReady());
            $order->customer->authUser->notify(new CustomerOrderIsReady($order->customer->id));
        }
        if(isset($changes['status']) && $order->status == 'accepted'){
            if($order->payment_status == null) return false;
            $order->invitation_time = null;
        }
        if(isset($changes['status']) && $order->status == 'on_his_way'){
            $order->customer->authUser->notify(new CustomerOrderOnHisWay($order->customer->id));
        }
        if(isset($changes['status']) && $order->status == 'delivered'){
            $order->customer->authUser->notify(new CustomerOrderDeliverd($order->customer->id));
        }
        if(isset($changes['status']) && $order->status == 'rejected'){
            $order->customer->authUser->notify(new CustomerOrderRejected($order->customer->id));
        }
        if(isset($changes['status']) && $order->status == 'pending'){

            if(isset($order->driver_id)){
                RejectedInvitation::create([
                    'order_id' => $order->id,
                    'driver_id' => $order->driver_id,
                ]);
            }
            $drivers = Driver::where('branch_id',$order->branch_id)->where('available',true)
            ->doesntHave('orders', 'and', function($q){
                $q->whereNotIn('status', ['delivered','rejected']);
            })->doesntHave('rejectedInvitations', 'and', function($q) use($order){
                $q->where('order_id', $order->id);
            })->get();
        
            $drivers_distances = array();
            
            foreach($drivers as $driver){
                $location = $driver->location;
                $drivers_distances[$driver->id] = resolve('distance')->calculate($order->branch->branchAddress->latitude,$order->branch->branchAddress->longitude, $location->latitude, $location->longitude);
            }

            asort($drivers_distances);
            $driver_id = array_key_first($drivers_distances);

            $allowed_distance = floatval(AppSetting::where('key','driver_branch_distance')->first()->value);

            if(!$driver_id || $allowed_distance < $drivers_distances[$driver_id]){
                $order->status = 'archived';
                $order->driver_id = null;
                $order->invitation_time = null;
                $this->returnPayment($order);
                event(new UpdateOrderStatusEvent($order));
                event(new DeleteOrderNodeEvent($order));
                return;
            }

            $order->status = "waiting_driver";
            $order->driver_id = $driver_id;
            $order->driver->authUser->notify(new DriverNewRequest());
            event(new UpdateOrderStatusEvent($order));

            $driver_timeout_minutes = intval(AppSetting::where('key','driver_timeout_minutes')->first()->value);
            $order->invitation_time = Carbon::now()->addMinutes($driver_timeout_minutes);
        }
        if(isset($changes['status']) && $order->status == 'closed'){
            event(new DeleteOrderNodeEvent($order));
            $order->status = $order->getOriginal()['status'];
        }
    }

    public function updated(Order $order)
    {
        //
    }

    
    public function deleted(Order $order)
    {
        //
    }

    
    public function restored(Order $order)
    {
        //
    }

    
    public function forceDeleted(Order $order)
    {
        //
    }

}
