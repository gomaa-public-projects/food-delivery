<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Guards\UserGuard;
use App\Guards\MyVoyagerGuard;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        if (request()->is('graphql')) //Remember To check voyager Policies
        {
            $this->registerPolicies();
        }

        Auth::extend('User_Guard', function ($app, $name, array $config) {
            $provider =  new $this->app['config']['auth.providers.'.$config['provider']]['model'];
            return new UserGuard($provider);
        });
        Auth::extend('My_Voyager_Guard', function ($app, $name, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\Guard...

            return new MyVoyagerGuard($name, request());
        });
        $this->app->singleton('VoyagerGuard', function () {
            return 'MyVoyagerGuard';
        });
    }
}
