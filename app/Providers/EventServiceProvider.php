<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'Nuwave\Lighthouse\Events\StartRequest' => [
            'App\Listeners\GraphQLQueryListener',
        ],
        'Nuwave\Lighthouse\Events\ManipulateResult' => [
            'App\Listeners\GraphQLResponseListener',
        ],
        'App\Events\PostRegisterEvent' => [
            'App\Listeners\PostRegisterHandler',
        ],
        'eloquent.creating: App\Branch' => [
            'App\Listeners\CreateBranchAuthUser',
        ],
        'eloquent.creating: App\Driver' => [
            'App\Listeners\CreateDriverAuthUser',
        ],
        'eloquent.created: App\Driver' => [
            'App\Listeners\CreateDriverNode',
        ],
        'eloquent.created: App\Order' => [
            'App\Listeners\CreateOrderNode',
        ],
        'App\Events\UpdateOrderStatusEvent' => [
            'App\Listeners\UpdateOrderStatusListener',
        ],
        'App\Events\DeleteOrderNodeEvent' => [
            'App\Listeners\DeleteOrderNodeListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
