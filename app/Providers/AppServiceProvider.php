<?php

namespace App\Providers;

use App\Order;
use App\Driver;
use App\Branch;
use App\AuthUser;
use App\Observers\OrderObserver;
use App\Observers\DriverObserver;
use App\Observers\BranchObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $calculator = new \App\Calculators\DistanceCalculator();
        $this->app->instance('distance', $calculator);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Order::observe(OrderObserver::class);
        Driver::observe(DriverObserver::class);
        Branch::observe(BranchObserver::class);

        Validator::extend('serve_all_day', function ($attribute, $value, $parameter, $validator) {
            if(!isset(request()->serve_all_day) && !$value){
                return false;
            }
            return true;
        });

        Validator::extend('delete_driver', function ($attribute, $value, $parameter, $validator) {
            $current_id = (int) str_replace('drivers/', '', strstr(url()->current(), 'drivers/'));
            if ($current_id != 0) {
                $driver = Driver::find($current_id);
                if ($driver->orders()->exists() || $driver->rejectedInvitations()->exists()) {
                    return false;
                }
            }
            return true;
        });

        Validator::extend('delete_branch', function ($attribute, $value, $parameter, $validator) {
            $current_id = (int) str_replace('branches/', '', strstr(url()->current(), 'branches/'));
            if ($current_id != 0) {
                $branch = Branch::find($current_id);
                if ($branch->drivers()->exists() || $branch->orders()->exists()) {
                    return false;
                }
            }
            return true;
        });

        Validator::extend('unique_driver', function ($attribute, $value, $parameter, $validator) {
        
            $auth = AuthUser::where('phone', "+966".substr($value, 1))->first();
            if ($auth) {
                return false;
            }
            return true;
        });

        Validator::extend('validate_password', function ($attribute, $value, $parameter, $validator) {
            $args = array("password" => $value);
            if(strpos(url()->previous(),"edit")){
                $validator = Validator::make($args, [
                    'password' => ['nullable', 'min:8'],
                ]);
                if ($validator->fails()) {
                    return false;
                }
            }
            if(strpos(url()->previous(),"create")){
                $validator = Validator::make($args, [
                    'password' => ['required', 'min:8'],
                ]);
                if ($validator->fails()) {
                    return false;
                }
            }
            return true;
        });

    }
}
