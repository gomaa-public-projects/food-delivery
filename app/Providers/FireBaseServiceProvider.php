<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Kreait\Firebase;
use Kreait\Firebase\Factory;

class FireBaseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Kreait\Firebase\Database',function ($app){
            $firebase = (new Factory)
            ->withDatabaseUri(config('firebase.FBDATABASE_URL'));
            $database = $firebase->createDatabase();       
            return $database; 
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
