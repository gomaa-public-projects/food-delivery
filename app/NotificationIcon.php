<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationIcon extends Model
{
    protected $fillable = [
        'notification_title',
        'icon_link',
    ];

    public function getIconLinkAttribute($value)
    {
        if (!request()->is('graphql'))
        {
            return $value;
        }
        if ($value == null) {return $value;}
        return asset('storage/' . $value);
    }
}
