<?php

namespace App;

use Carbon\Carbon;
use App\Traits\LocationTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Driver extends \Akwad\VoyagerExtension\Models\User
{
    use LocationTrait;

    protected $fillable = [
        'name',
        'phone',
        'password',
        'auth_user_id',
        'branch_id',
        'available',
        'driver_node_id',
        'delete',
    ];
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }
    public function rejectedInvitations(): HasMany
    {
        return $this->hasMany(RejectedInvitation::class);
    }
    public function branch(): BelongsTo
    {
        return $this->belongsTo(Branch::class);
    }
    public function authUser(): BelongsTo
    {
        return $this->belongsTo(AuthUser::class);
    }
    public function getPhoneAttribute(){
        if(isset($this->authUser)){
            return $this->authUser->phone;
        }
        return NULL;
    }
    public function setPasswordAttribute($value){
        if(isset($this->authUser)){
            $this->authUser->password = $value;
            $this->authUser->save();
            return;
        }
        $this->attributes['password'] = $value;
    }
    public function getLocationAttribute(){
        return $this->getLocation();
    }

    public function getProfitsAttribute(){
        return $this->orders()->whereDate('created_at', ">=",Carbon::now()->subDays($this->profits_days)->toDateString())->whereDate('created_at',"<=",Carbon::now()->toDateString())->get()->sum('delivery');
    }

    public function getProfitsDaysAttribute(){
        return intval(AppSetting::where('key','driver_profits_days')->first()->value);
    }
}
