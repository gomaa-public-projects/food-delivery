<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use App\Events\UpdateOrderStatusEvent;

class UpdateOrderStatusListener
{
    public $database;

    public function __construct(\Kreait\Firebase\Database $database)
    {
        $this->database = $database;
    }

    public function handle(UpdateOrderStatusEvent $event)
    {
        Log::channel('listeners')->info("UpdateOrderStatusListener: listener in progress");

        $order = $event->order;

        $ref = "orders/$order->order_node_id";

        $oldMetaData = $this->database->getReference($ref)->getValue();
        $oldMetaData["meta-data"]['status'] = $order->status;
        $oldMetaData["meta-data"]['payment_method'] = $order->payment_method;
        $oldMetaData["meta-data"]['branch_id'] = $order->branch_id;
        $data = $oldMetaData["meta-data"];
        $this->updateNode($ref, $data);
        Log::channel('listeners')->info("UpdateOrderStatusListener: updated order $order->id status at FB $ref");
        Log::channel('listeners')->info("UpdateOrderStatusListener: listener has finished");
    }
    
    
    public function updateNode($reference, $meta_data)
    {
        $database = $this->database;
        $database
            ->getReference($reference)
            ->set([
                "meta-data" => $meta_data
            ]);
    }

    public function failed($event, $exception)
    {
        //
    }
}
