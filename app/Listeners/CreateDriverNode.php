<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;



class CreateDriverNode
{
    public $database;

    public function __construct(\Kreait\Firebase\Database $database)
    {
        $this->database = $database;
    }

    public function handle($driver)
    {

        $reference = 'drivers';
        $model_field = 'driver_node_id';

        Log::channel('listeners')->info("CreateDriverNode: with id: $driver->id listener in progress");
        
        $data = [
            "longitude" => isset($driver->branch->address)? $driver->branch->address->longitude : 0,
            "latitude" => isset($driver->branch->address)? $driver->branch->address->latitude : 0,
            "driver_id" => $driver->id
        ];


        $new_node = $this->createNode($reference, $data);
        $driver->update([$model_field => $new_node]);
        
        Log::channel('listeners')->info("CreateDriverNode: listener has finished");
    }

    public function createNode($reference, $data){
        $newNode = $this->database
        ->getReference($reference)
        ->push([
            "meta-data"=> $data,
        ]);
        return explode("/",$newNode)[4];
    }

    public function failed($event, $exception)
    {
        //
    }
}
