<?php

namespace App\Listeners;

use App\Address;
use App\Customer;
use App\Events\PostRegisterEvent;
use Illuminate\Support\Facades\Log;

class PostRegisterHandler
{
    public $queue = 'listeners';
    public $timeout = 10;

    public function __construct()
    {
        //
    }

    public function handle(PostRegisterEvent $event)
    {
        $auth_user = $event->auth_user;
        $extras = $event->extras;

        Log::channel('listeners')->info("CreateCustomerProfile: listener in progress");
        Log::channel('listeners')->info("CreateCustomerProfile: creating customer for auth_user $auth_user->id");
        $customer = Customer::create([
            "username" => $extras["username"],
            "auth_user_id" => $auth_user->id,
        ]);
        $address = Address::create([
            "longitude" => $extras['longitude'],
            "latitude" => $extras['latitude'],
            "description" => $extras['description'],
            "customer_id" => $customer->id,
        ]);
        Log::channel('listeners')->info("CreateCustomerProfile: Created Customer with id $customer->id");
        Log::channel('listeners')->info("CreateCustomerProfile: Created Address with id $address->id");
        Log::channel('listeners')->info("CreateCustomerProfile: listener has finished");
    }

    public function failed(PostRegisterEvent $event, $exception)
    {
        //
    }
}
