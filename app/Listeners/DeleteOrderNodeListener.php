<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use App\Events\DeleteOrderNodeEvent;

class DeleteOrderNodeListener
{
    public $database;

    public function __construct(\Kreait\Firebase\Database $database)
    {
        $this->database = $database;
    }

    public function handle(DeleteOrderNodeEvent $event)
    {
        Log::channel('listeners')->info("DeleteOrderNodeListener: listener in progress");

        $order = $event->order;

        $ref = "orders/$order->order_node_id";

        $this->deleteNode($ref);

        Log::channel('listeners')->info("DeleteOrderNodeListener: deleted order $order->id node at FB $ref");
        Log::channel('listeners')->info("DeleteOrderNodeListener: listener has finished");
    }
    
    

    public function deleteNode($reference)
    {
        $this->database->getReference($reference)->remove();
    }

    public function failed($event, $exception)
    {
        //
    }
}
