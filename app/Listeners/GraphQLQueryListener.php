<?php

namespace App\Listeners;

use Nuwave\Lighthouse\Events\StartRequest;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class GraphQLQueryListener 
{
    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    public $queue = 'listeners';
    public $timeout = 10;
    public $actor_id,$actor_type;
    public $length = 1500;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param   $event
     * @return void
     */
    public function handle(StartRequest $event)
    {
        $result = $event->request->query();

        if(strpos($event->request->query(), '__schema') !== false){
            return;
        }

        $type = strpos($result, 'mutation')!== false? "Mutation" : "Query";

        Log::channel('graphQL_Queries')->info("------------- Graph QL {$type} -------------");
        Log::channel('graphQL_Queries')->info(substr($event->request->query(), 0, $this->length));
        Log::channel('graphQL_Queries')->info("-------------- End of {$type} --------------");
    }


    /**
     * Handle a job failure.
     *
     * @param  \App\Events\OrderShipped  $event
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(StartRequest $event, $exception)
    {
        //
    }
}
