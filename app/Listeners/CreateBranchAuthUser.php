<?php

namespace App\Listeners;
use App\AuthUser;
use Illuminate\Support\Facades\Log;



class CreateBranchAuthUser
{

    public $queue = 'listeners';
    public $timeout = 10;

    public function handle($user)
    {
        $AuthUser= AuthUser::create([
            'type' => 'email',
            'email' => $user->getAttributes()['email'],
            'password' => $user->getAttributes()['password'],
            'token' => null,
            'role' => 'branch'
        ]);
        $user->email = null;
        $user->password = null;
        $user->auth_user_id = $AuthUser->id;
        
        
        Log::channel('listeners')->info("CreateBranchAuthUser: listener has finished");
    }

    public function failed($event, $exception)
    {
        //
    }
}
