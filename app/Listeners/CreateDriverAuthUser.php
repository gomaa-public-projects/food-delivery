<?php

namespace App\Listeners;
use App\AuthUser;
use Illuminate\Support\Facades\Log;



class CreateDriverAuthUser
{

    public $queue = 'listeners';
    public $timeout = 10;

    public function handle($user)
    {
        $phone = $user->getAttributes()['phone'];
        $phone = "+966".substr($phone, 1); 
        $AuthUser= AuthUser::create([
            'type' => 'phone',
            'phone' => $phone,
            'password' => $user->getAttributes()['password'],
            'token' => null,
            'role' => 'driver'
        ]);
        $user->phone = null;
        $user->password = null;
        $user->auth_user_id = $AuthUser->id;
        
        
        Log::channel('listeners')->info("CreateDriverAuthUser: listener has finished");
    }

    public function failed($event, $exception)
    {
        //
    }
}
