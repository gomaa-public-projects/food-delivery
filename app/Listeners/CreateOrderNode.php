<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;



class CreateOrderNode
{
    public $database;

    public function __construct(\Kreait\Firebase\Database $database)
    {
        $this->database = $database;
    }

    public function handle($order)
    {

        $reference = 'orders';
        $model_field = 'order_node_id';

        Log::channel('listeners')->info("CreateOrderNode: with id: $order->id listener in progress");
        $data = new \stdClass;
        $data->order_id = $order->id;
        $data->status = $order->status;
        $data->branch_id = $order->branch_id;
        $data->payment_method = $order->payment_method;

        $new_node = $this->createNode($reference, $data);
        $order->update([$model_field => $new_node]);
        
        Log::channel('listeners')->info("CreateOrderNode: listener has finished");
    }

    public function createNode($reference, $data){
        $newNode = $this->database
        ->getReference($reference)
        ->push([
            "meta-data"=> $data,
        ]);
        return explode("/",$newNode)[4];
    }

    public function failed($event, $exception)
    {
        //
    }
}
