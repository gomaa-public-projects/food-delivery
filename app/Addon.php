<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Addon extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'price',
        'code',
        'name',
        'image_link',
        'food_item_id',
    ];
    public function getImageLinkAttribute($value)
    {
        if (!request()->is('graphql'))
        {
            return $value;
        }
       
        if ($value == null) {return $value;}
        return asset('storage/' . $value);
    }
    public function foodItem(): BelongsTo
    {
        return $this->belongsTo(FoodItem::class)->withTrashed();
    }

}
