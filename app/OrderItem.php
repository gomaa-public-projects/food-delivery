<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class OrderItem extends Model
{
    protected $fillable = [
        'order_id',
        'food_item_id',
        'count',
        'item_size_id',
    ];

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function foodItem(): BelongsTo
    {
        return $this->belongsTo(FoodItem::class)->withTrashed();
    }

    public function addons(): BelongsToMany
    {
        return $this->belongsToMany(Addon::class)->withTrashed();
    }
    public function itemSize(): BelongsTo
    {
        return $this->belongsTo(ItemSize::class);
    }
    public function getPriceAttribute(){
        $price = 0;
        if($this->foodItem){
            $price = $this->foodItem->price;
        }
        if($this->itemSize){
            $price = $this->itemSize->price;
        }
        if($this->addons != []){
            $price += $this->addons->sum('price');
        }
        return $price * $this->count;
    }
}
