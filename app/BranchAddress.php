<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BranchAddress extends Model
{
    protected $fillable = [
        "longitude",
        "latitude",
        "description",
        "branch_id",
        "coordinates"
    ];

    public function branch(): BelongsTo
    {
        return $this->belongsTo(Branch::class);
    }
    
    public function getLangAndLong($value){
        if($value){
            $clear = trim(preg_replace('/[a-zA-Z\(\)]/', '', $value->getValue()));
            if (!empty($clear)) {
                $clear = str_replace('_','',$clear);
                $clear = str_replace('\'','',$clear);
                    list($longitude,$latitude) = explode(' ', $clear);
            }
            $this->attributes['coordinates'] = null;
            $this->attributes['latitude'] = $latitude;
            $this->attributes['longitude'] = $longitude;
        }
    }
    public function setCoordinatesAttribute($value){
        $this->getLangAndLong($value);
    }

    public function getCoordinates()
    {
        $coordinates = array();
        $coordinates[] = array(
            'lat' => $this->latitude,
            'lng' => $this->longitude,
        );

        if(!$this->latitude || !$this->longitude){
            $coordinates[0] = array(
                'lat' => config('voyager.googlemaps.center.lat'),
                'lng' => config('voyager.googlemaps.center.lng'),
            );
        }

        return $coordinates;
        
    }

    public function getCoordinatesAttribute()
    {
        
    }
}
