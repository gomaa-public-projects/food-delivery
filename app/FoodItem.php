<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodItem extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'describtion',
        'image',
        'thumbnail_image',
        'price',
        'category_id',
        'restaurant_code',
    ];

    public function getThumbnailImageAttribute($value)
    {
        if (!request()->is('graphql'))
        {
            return $value;
        }
       
        if ($value == null) {return $value;}
        return asset('storage/' . $value);
    }

    public function getImageAttribute($value)
    {
        if (!request()->is('graphql'))
        {
            return $value;
        }
       
        if ($value == null) {return $value;}
        return asset('storage/' . $value);
    }
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class)->withTrashed();
    }
    public function addons(): HasMany
    {
        return $this->hasMany(Addon::class);
    }
    public function itemSizes(): HasMany
    {
        return $this->hasMany(ItemSize::class);
    }
    public function getAvailableAttribute(){
        if(!$this->category->from || !$this->category->to){
            return true;
        }
        $now = Carbon::now()->setTimezone(config("app.TIMEZONE"))->toTimeString();
        $from = Carbon::createFromFormat('H:i:s',$this->category->from)->toTimeString();
        $to = Carbon::createFromFormat('H:i:s',$this->category->to)->toTimeString();
    
        return ($from <= $now && $now <= $to) || ($to < $from && $from < $now && $to < $now) || ($to < $from && $from > $now && $to > $now) || $this->category->serve_all_day;
    }
    public function getServingTimeAttribute(){
        if(!$this->category->from || !$this->category->to || $this->category->serve_all_day){
            return "متاح دائما";
        }
        $from = Carbon::createFromFormat('H:i:s',$this->category->from);
        $to = Carbon::createFromFormat('H:i:s',$this->category->to);

        $FromResult = $this->GetArabicTime($from);
        $ToResult = $this->GetArabicTime($to);

        return "متاح من ".$FromResult." - ".$ToResult;
    }

    public function GetArabicTime($time){
        $posttime_a = $time->format('A');
        $posttime_i = $time->format('i');
        $posttime_h = $time->format('g');

        $ampm = array("AM", "PM");
        $ampmreplace = array("ص", "م");
        $ar_ampm = str_replace($ampm, $ampmreplace, $posttime_a);
    
        $standardletters = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $eastern_arabic_letters = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
        $post_time = $posttime_h . ':' . $posttime_i." ".$ar_ampm;
        $arabic_time = str_replace($standardletters, $eastern_arabic_letters, $post_time);
    
        return $arabic_time;
    }
}
