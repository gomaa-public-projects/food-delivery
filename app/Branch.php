<?php

namespace App;

use Akwad\VoyagerExtension\Facades\Voyager;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Branch extends \Akwad\VoyagerExtension\Models\User
{
    protected $fillable = [
    'auth_user_id',
    'name',
    'phone',
    'email',
    'password',
    'delete',
    'coordinates',
    'description',
    "longitude",
    "latitude",
    ];

    public function getRoleIdAttribute($value)
    {
        return Voyager::modelClass('Role')::where('name',$this->authUser->role)->first()->id;
    }

    public function authUser():BelongsTo
    {
        return $this->belongsTo(AuthUser::class);
    }
    public function branchAddress(): HasOne
    {
        return $this->hasOne(BranchAddress::class,'branch_id', 'id');
    }
    public function drivers(): HasMany
    {
        return $this->hasMany(Driver::class);
    }
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    public function getEmailAttribute()
    {
        if(isset($this->authUser)){
            return $this->authUser->email;
        }
        return NULL;
    }

    public function setPasswordAttribute($value){
        if(isset($this->authUser)){
            $this->authUser->password = $value;
            $this->authUser->save();
            return;
        }
        $this->attributes['password'] = $value;
    }

    public function getLangAndLong($value){
        if($value){
            $clear = trim(preg_replace('/[a-zA-Z\(\)]/', '', $value->getValue()));
            if (!empty($clear)) {
                $clear = str_replace('_','',$clear);
                $clear = str_replace('\'','',$clear);
                    list($longitude,$latitude) = explode(' ', $clear);
            }
            $this->attributes['coordinates'] = null;
            $this->attributes['latitude'] = $latitude;
            $this->attributes['longitude'] = $longitude;
        }
    }
    public function setCoordinatesAttribute($value){
        $this->getLangAndLong($value);
    }

    public function getCoordinates()
    {
        if(isset($this->branchAddress)){
            return $this->branchAddress->getCoordinates();
        }
        return NULL;
    }
}
