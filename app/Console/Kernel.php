<?php

namespace App\Console;
use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function(){
            $this->updateOrders();
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
    public function updateOrders(){
        $orders = DB::table('orders')->where('status','waiting_driver')->whereTime('invitation_time','<=',Carbon::now()->toTimeString());
        if($orders->pluck('id')->toArray()!= []){
            Log::info("Updating Orders Started...");
            foreach($orders->get() as $order){
                Order::find($order->id)->update(['status' => 'pending']);
            }
            $ids = implode(',',$orders->pluck('id')->toArray());
            Log::info("Updating Orders of IDs: ".$ids);
            Log::info("Updating Orders Finished...");
        }
    }
}
