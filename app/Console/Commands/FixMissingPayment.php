<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;

class FixMissingPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::whereNull('payment_method')->get();
        $this->info("found ".count($orders)." orders");
        foreach ($orders as $order){
            if($order->customer && $order->customer->payment_option){
                $order->update(['payment_method' => $order->customer->payment_option]);
                $this->info("Updated Order ID: $order->id with : ".$order->customer->payment_option);
                continue;
            }
            $order->update(['payment_method' => 'VISA']);
            $this->info("Updated Order ID: $order->id with : VISA");
        }
    }
}
