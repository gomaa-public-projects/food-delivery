<?php

namespace App\Http\Livewire;

use Livewire\Component;

class FirebaseTool extends Component
{
    public $order_id,
    $order_result, 
    $order_error, 
    $driver_id,
    $driver_result, 
    $driver_error;

    public function render()
    {
        return view('developers.firebase-tool');
    }

    public function goToOrder(){
        $this->order_error = false;
        $order = \App\Order::find($this->order_id);
        $node = $order? $order->order_node_id : null;
        if(!$order || !$node){
            $this->order_error = true;
            return;
        }

        $this->emit('goToLink', "https://console.firebase.google.com/u/0/project/mama-noura-app/database/mama-noura-app/data/orders/".$node);
    }

    public function goToDriver(){
        $this->driver_error = false;
        $driver = \App\Driver::find($this->driver_id);
        $node = $driver? $driver->driver_node_id : null;
        if(!$driver || !$node){
            $this->driver_error = true;
            return;
        }

        $this->emit('goToLink', "https://console.firebase.google.com/u/0/project/mama-noura-app/database/mama-noura-app/data/drivers/".$node);
    }

    public function updatedDriverID(){
        $this->driver_error = false;
        $driver = \App\Driver::find($this->driver_id);
        if(!$driver){
            $this->driver_error = true;
            $this->driver_result = null;
            return;
        }
        $this->driver_result = "Found Driver Of ID: ".$this->driver_id;
    }

    public function updatedOrderID(){
        $this->order_error = false;
        $order = \App\Order::find($this->order_id);
        if(!$order){
            $this->order_error = true;
            $this->order_result = null;
            return;
        }
        $this->order_result = "Found Order Of ID: ".$this->order_id;
    }
}
