<?php

namespace App\Http\Controllers;

use App\Order;
use App\Customer;
use Carbon\Carbon;
use \DomainException;
use \Firebase\JWT\JWT;
use App\BlackListToken;
use \InvalidArgumentException;
use \UnexpectedValueException;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PaymentController extends Controller
{
    private $access_code, $merchant_identifier, $request_phrase, $sandbox;
    public function __construct()
    {
        $this->access_code = config("amazonPayments.ACCESS_CODE");
        $this->merchant_identifier = config("amazonPayments.MERCHANT_ID");
        $this->request_phrase = config("amazonPayments.REQUEST_PHRASE");
        $this->currency = config("amazonPayments.CURRENCY");
        $this->language = config("amazonPayments.LANGUAGE");
        $this->sandbox = config("amazonPayments.SANDBOX")? "sb":"";
    }

    /**
     * Get Credit Card token and save it.
     *
     * @return void
     */
    public function getPaymentInfo()
    {
        Log::channel('payments')->info("PaymentController (getPaymentInfo): controller in progress");
        request()->session()->forget('jwt_token');
        if(!request()->has('jwt_token')){
            Log::channel('payments')->info("PaymentController (getPaymentInfo): token is missing");
            return redirect("result?success=false&error=php&message=TokenIsmissig");
        }

        $jwt_token = request()->jwt_token;
        $customer = $this->Guard($jwt_token, true);
        if(is_string($customer)){
            request()->session()->forget('jwt_token');
            request()->session()->forget('order_id');
            Log::channel('payments')->info("PaymentController (getPaymentInfo): Guard Error: {$customer}");
            return redirect("result?success=false&error=php&message=".$customer);
        }

        return $this->cardAuthorize($customer);
    }

    /**
     * Execute a simple one time purchase
     * to get the card token.
     * @return card details
     */
    public function cardAuthorize($customer){
        Log::channel('payments')->info("PaymentController (cardAuthorize): Controller has started");
        Log::channel('payments')->info("PaymentController (cardAuthorize): Customer ID: ".$customer->id);
        $price = 100;
        Log::channel('payments')->info("PaymentController (cardAuthorize): Amount: ".$price);
        $payment_reference = "customer_{$customer->id}_id_".Carbon::now()->format('h_i_s');

        $arrData = array(
        'access_code' => $this->access_code,
        'currency' => $this->currency,
        'language' => $this->language,
        'merchant_identifier' => $this->merchant_identifier,
        'merchant_reference' => "$payment_reference",
        'command' => "PURCHASE",
        'amount' => "$price",
        'return_url' => env("APP_URL")."/cardAuthorizeResult?customer_id=".$customer->id,
        'customer_email' => $customer->email??"{$customer->id}_@food.com"
        );

        ksort($arrData);

        $data = json_decode(json_encode($arrData, JSON_FORCE_OBJECT));

        $data->signature = $this->createSignature($arrData, "sha256");
        Log::channel('payments')->info("PaymentController (cardAuthorize): Data Created, Redircting to cardAuthorizeResult");

        return view("payment.purchase",['data' => $data, 'action' => "https://{$this->sandbox}checkout.PayFort.com/FortAPI/paymentPage"]);
    }

    /**
     * handles the response from cardAuthorize request
     * and saves data to database.
     * then cancel the payment to return value back to client
     * @return redirects to success
     */
    public function cardAuthorizeResult(){
        Log::channel('payments')->info("PaymentController (cardAuthorizeResult): Controller Has Started");
        $result = request()->all();
        
        if($result['response_code'] == "00003"){
            Log::channel('payments')->info("PaymentController (cardAuthorizeResult): error with code: ". $result['response_code']);
            return redirect("result?success=false&message=هذا الكارت غير مدعوم");
        }
        if($result['response_code'] != "14000"){
            Log::channel('payments')->info("PaymentController (cardAuthorizeResult): error with code: ". $result['response_code']);
            return redirect("result?success=false&message=تم إلغاء عملية الدفع");
        }

        Log::channel('payments')->info("PaymentController (cardAuthorizeResult): Passed error Check");
        $this->logResult($result, "cardAuthorizeResult");
        $customer_id = request()->customer_id;
        $customer = Customer::findOrFail($customer_id);
        Log::channel('payments')->info("PaymentController (cardAuthorizeResult): Updating Customer Of ID: ".$customer->id);
        if(isset($result['token_name'])){
            $customer->update([
                'customer_token' => $result['token_name'],
                'card_number' => $result['card_number'],
                'payment_option' => $result['payment_option']
            ]);
            Log::channel('payments')->info("PaymentController (cardAuthorizeResult): updated Customer Of ID: ".$customer->id);
        }
        Log::channel('payments')->info("PaymentController (cardAuthorizeResult): controller has finished");

        $this->cardReturn($result["merchant_reference"], $result['fort_id'], $customer->id);
        return redirect("result?success=true");
    }

    /**
     * cancel the payment to return value back to client
     * @return card details
     */
    public function cardReturn($merchant_reference, $fort_id){
        Log::channel('payments')->info("PaymentController (cardReturn): controller has started");
        Log::channel('payments')->info("PaymentController (cardReturn): merchant_reference: ". $merchant_reference);
        $price = 100;
        Log::channel('payments')->info("PaymentController (cardReturn): Amount: ". $price);
        $arrData = array(
        "command" => "REFUND",
        "access_code" => $this->access_code,
        'amount' => "$price",
        'currency' => $this->currency,
        "language" => $this->language,
        "merchant_identifier" => $this->merchant_identifier,
        "merchant_reference" => $merchant_reference,
        "fort_id" => $fort_id
        );

        ksort($arrData);
        $data = json_decode(json_encode($arrData, JSON_FORCE_OBJECT));
        $data->signature = $this->createSignature($arrData, "sha256");
        Log::channel('payments')->info("PaymentController (cardReturn): Data Ready, sending");
        $response = $this->curlExecution(json_encode($data), "https://{$this->sandbox}paymentservices.payfort.com/FortAPI/paymentApi");
        $this->logResult($response, "cardReturn");
    }

    /**
     * Pay With Saved Card Token.
     *
     * @return void
     */
    public function pay()
    {
        Log::channel('payments')->info("PaymentController (pay): controller in progress");
        Log::channel('payments')->info("PaymentController (pay): paying order of ID: ".request()->order_id);
        $order = \App\Order::find(request()->order_id);
        if(!$order->customer->has_payment_info){
            Log::channel('payments')->info("PaymentController (purchaseResult): error");
            $order->status = 'archived';
            $order->save();
            return redirect("result?success=false&message=عفوا! بيانات الدفع غير مكتملة");
        }
        Log::channel('payments')->info("PaymentController (pay): finished, redirecting to payWithToken");
        return $this->payWithToken($order);
    }

    /**
     * Excute payment with card.
     *
     * @return result json || Error
     */
    public function payWithToken($order){
        Log::channel('payments')->info("PaymentController (payWithToken): controller has started");
        Log::channel('payments')->info("PaymentController (payWithToken): Order ID: ". $order->id);
        $price = intval($order->total_price * 100);
        Log::channel('payments')->info("PaymentController (payWithToken): Amount: ". $price);
        $payment_reference = "order_{$order->id}_id_".Carbon::now()->format('h_i_s');
        $arrData = array(
        'access_code' => $this->access_code,
        'currency' => $this->currency,
        'language' => $this->language,
        'merchant_identifier' => $this->merchant_identifier,
        'merchant_reference' => "$payment_reference",
        'command' => "PURCHASE",
        'token_name' => "{$order->customer->customer_token}",
        'amount' => "$price",
        'return_url' => env("APP_URL")."/payWithTokenResult?order_id=".$order->id,
        'customer_email' => $order->customer->email??"{$order->customer->id}_@food.com"
        );

        $order->update(['payment_reference' => $payment_reference]);

        ksort($arrData);

        $data = json_decode(json_encode($arrData, JSON_FORCE_OBJECT));

        $data->signature = $this->createSignature($arrData, "sha256");

        Log::channel('payments')->info("PaymentController (payWithToken): Data Created, Redirecting to payWithTokenResult");

        return view("payment.purchase",['data' => $data, 'action' => "https://{$this->sandbox}checkout.PayFort.com/FortAPI/paymentPage"]);
    }

    /**
     * Handles The Result of payWithToken execution.
     *
     * @return view
     */
    public function payWithTokenResult(){
        Log::channel('payments')->info("PaymentController (payWithTokenResult): Controller Has Started");
        $result = request()->all();
        $this->logResult($result, "payWithTokenResult");

        if($result['response_code'] == "00003"){
            Log::channel('payments')->info("PaymentController (payWithTokenResult): error with code: ". $result['response_code']);
            return redirect("result?success=false&message=هذا الكارت غير مدعوم");
        }
        if($result['response_code'] != "14000"){
            Log::channel('payments')->info("PaymentController (payWithTokenResult): error with code: ". $result['response_code']);
            return redirect("result?success=false&message=تم إلغاء عملية الدفع");
        }

        Log::channel('payments')->info("PaymentController (payWithTokenResult): passed error check");
        $order_id = request()->order_id;
        $order = Order::findOrFail($order_id);

        $order->update([
            'payment_transaction_id' => $result['fort_id'],
            'authorization_code' => $result['authorization_code'],
            'payment_method' => isset($result['payment_option'])? $result['payment_option'] : null,
            'payment_status' => 'paid',
            'status' => 'pending',
        ]);
        Log::channel('payments')->info("PaymentController (payWithTokenResult): updated Order of ID: ".$order->id);
        Log::channel('payments')->info("PaymentController (payWithTokenResult): controller has finished");
        return redirect("result?success=true");
    }


    // public function capturePayment($order){
    //     $price = intval($order->total_price * 100);
    //     $arrData = array(
    //     "command" => "CAPTURE",
    //     "access_code" => $this->access_code,
    //     "language" => $this->language,
    //     'amount' => "$price",
    //     "merchant_identifier" => $this->merchant_identifier,
    //     "merchant_reference" => $order->payment_reference,
    //     "fort_id" => $order->payment_transaction_id
    //     );
    //     ksort($arrData);
    //     $order->payment_status = "paid";
    //     $data = json_decode(json_encode($arrData, JSON_FORCE_OBJECT));
    //     $data->signature = $this->createSignature($arrData, "sha256");
    //     $response = $this->curlExecution(json_encode($data), "https://sbpaymentservices.payfort.com/FortAPI/paymentApi");
    //     $this->logResult($response, "capturePayment");
    // }

    public function returnPayment($order){
        Log::channel('payments')->info("PaymentController (returnPayment): controller has started");
        Log::channel('payments')->info("PaymentController (returnPayment): order of ID: ". $order->id);
        $price = intval($order->total_price * 100);
        Log::channel('payments')->info("PaymentController (returnPayment): Amount: ". $price);
        $arrData = array(
        "command" => "REFUND",
        "access_code" => $this->access_code,
        'currency' => $this->currency,
        'language' => $this->language,
        'amount' => "$price",
        "merchant_identifier" => $this->merchant_identifier,
        "merchant_reference" => $order->payment_reference,
        "fort_id" => $order->payment_transaction_id
        );

        ksort($arrData);
        $order->payment_status = "cancelled";
        $data = json_decode(json_encode($arrData, JSON_FORCE_OBJECT));
        $data->signature = $this->createSignature($arrData, "sha256");
        Log::channel('payments')->info("PaymentController (returnPayment): Data Ready, sending");
        $response = $this->curlExecution(json_encode($data), "https://{$this->sandbox}paymentservices.payfort.com/FortAPI/paymentApi");
        $this->logResult($response, "returnPayment");
    }

    public function directTransactionResult(){
        $result = request()->all();
        $this->logResult($result, "directTransactionResult");
    }

    public function paymentNotification(){
        $result = request()->all();
        $this->logResult($result, "paymentNotification");
    }

    /**
     * general function used to show success or error.
     *
     * @return view
     */
    public function result()
    {
        return view('payment.result');
    }

    public function Guard($jwt_token, $returnCustomer = false){
        $secretKey = \env("JWT_SECRET");
        $secretKey = base64_decode($secretKey);


        $black_Listed = BlackListToken::where('token', $jwt_token)->first();       

        if($black_Listed){
            return 'Token is BlackListed!!';
        }
 

        try
        {
            JWT::$leeway = 60 * 60 * 24; //24 hour
            $data=JWT::decode($jwt_token, $secretKey, ['HS512']);        
        }
        catch (DomainException | InvalidArgumentException | UnexpectedValueException  $e)
        {
            return 'Token is Invalid!!';
        } 
        $auth_data = $data->data;
        try {
            $customer = Customer::where('auth_user_id',$auth_data->id)->firstOrFail();     
        }
        catch (ModelNotFoundException $e)
        {
            return 'Customer Not Found';
        }
        if($returnCustomer){
            return $customer;
        } 
    }

    public function curlExecution($payload, $url){
        $ch = curl_init();
        $headers = array("Content-Type:application/json");
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        $result = curl_exec($ch);
        curl_close($ch);
        $result=explode("\r\n",$result);
        $result=json_decode(end($result));

        return $result;
    }

    public function createSignature($arrData, $encryption_type){
        $request_phrase = $this->request_phrase;
        $shaString  = '';
        foreach ($arrData as $key => $value) {
            $shaString .= "$key=$value";
        }
        $shaString =  $request_phrase. $shaString. $request_phrase;
        return hash($encryption_type, $shaString);
    }

    public function logResult($result, $function = "Default"){
        Log::channel('payments')->info("PaymentController (logResult): from function: ".$function);
        $parameters = ["amount", "response_code", "card_number", "customer_ip", "customer_id","response_message","merchant_reference", "status"];
        $toLog = new \stdClass;

        if(is_array($result)){
            $result = json_decode(json_encode($result, JSON_FORCE_OBJECT));
        }
        foreach($parameters as $parameter){
            if(isset($result->{$parameter})){
               $toLog->{$parameter} = $result->{$parameter};
            }
        }
        Log::channel('payments')->info("PaymentController (logResult): Response: ".json_encode($toLog));
    }
}
