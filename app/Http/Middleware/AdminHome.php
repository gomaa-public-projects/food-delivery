<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Akwad\VoyagerExtension\Models\Role;

class AdminHome
{
    protected $allowed_roles = ['admin' , 'branch'];
    public function handle($request, Closure $next)
    {
        // if(Auth::guard('MyVoyagerGuard')->user()){
        //     $role_id = Auth::guard('MyVoyagerGuard')->user()->role_id;
        //     $name = Role::find($role_id)->name;
        //     if(str_replace(env('APP_URL'),'',url()->current())=="/admin" && !in_array($name, $this->allowed_roles)) return redirect('admin/food-items');
        // }
        
        return $next($request);
    }
}
