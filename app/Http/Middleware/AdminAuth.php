<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Akwad\VoyagerExtension\Models\Role;

class AdminAuth
{
    protected $allowed_roles = ['admin'];
    public function handle($request, Closure $next)
    {

        if(!Auth::guard('MyVoyagerGuard')->user()){
            return redirect('admin/login');
        }

        $role_id = Auth::guard('MyVoyagerGuard')->user()->role_id;
        $name = Role::find($role_id)->name;
        if(!in_array($name, $this->allowed_roles)){
            return redirect('admin');
        }

        return $next($request);
    }
}
