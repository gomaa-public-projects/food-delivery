<?php

namespace App\Http\Middleware;

use App\AuthUser;
use App\Exceptions\LightHouseCustomException;
use App\BlackListToken;
use Closure;
use \Firebase\JWT\JWT;
use Illuminate\Support\Facades\Log;

class MyCustomAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $userTokenHeader = $request->header('Authorization');

        if (!$userTokenHeader) {
            /*
             * The request lacks the authorization token
             */
            throw new LightHouseCustomException(403, "The request lacks the authorization token");
        }
        /*
         * Extract the jwt from the Bearer
         */
        list($jwtToken) = sscanf($userTokenHeader, 'Bearer %s');

        if (!$jwtToken) {
            /*
             * No token was able to be extracted from the authorization header
             */
            throw new LightHouseCustomException(403, "No token was able to be extracted from the authorization header");
        }


        $black_listed = BlackListToken::where('token', $jwtToken)->first();
        if($black_listed) {
            Log::channel('guards')->info("UserGuard: Token is BlackListed");
            throw new LightHouseCustomException(401, 'Token is BlackListed!!');
        }


        /*
             * decode the jwt using the key from config
             */

        $secretKey = config("authentication.JWT_SECRET");
        $secretKey = base64_decode($secretKey);
        $jwt = JWT::decode($jwtToken, $secretKey, array('HS512'));
        $userObject = $jwt->data;
        // check existance of that user in Database
        $user = AuthUser::where('email', '=', $userObject->email)->first();
        if ($user === null) {
            // user doesn't exist
            throw new LightHouseCustomException(403, "This user doesn't exist");
        }
        // user found
        //add user to request to pass it to ReAuthMutator
        $user = $user->toArray();
        //check if email verified
        if ($user["email_verified_at"] == null) {
            throw new LightHouseCustomException(403, "User email isn't verified yet");
        }
        return $next($request->merge(['authUser' => $user]));
    }
}
