<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrequentlyAskedQuestion extends Model
{
    protected $fillable = [
        'question',
        'answer',
    ];
}
