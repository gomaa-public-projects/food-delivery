<?php

namespace App\GraphQL\Mutations;

use App\AuthUser;
use App\Events\PostRegisterEvent;
use App\BlackListToken;
use App\Exceptions\LightHouseCustomException;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use \Firebase\JWT\JWT;
use Illuminate\Support\Facades\Auth;
use Twilio\Rest\Client;


class RegisterMutator
{
    public function sendOTP($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo){
       
        if($args["type"] == "signup"){
            $validator = Validator::make($args, [
                'phone' => ['unique:auth_users'],
            ]);
            if ($validator->fails()) {
                Log::channel('authentication')->info("RegisterMutator (sendOTP): {$validator->messages()->first()}");
                throw new LightHouseCustomException(403, "{$validator->messages()->first()}");
            }
        }

        if($args["type"] == "forget"){
            $validator = Validator::make($args, [
                'phone' => ['exists:auth_users'],
            ]);
            if ($validator->fails()) {
                Log::channel('authentication')->info("RegisterMutator (sendOTP): {$validator->messages()->first()}");
                throw new LightHouseCustomException(403, "{$validator->messages()->first()}");
            }
        }

        $twilio = new Client(config('sms.ACCOUNT_SID'),config('sms.AUTH_TOKEN'));
        $verification = '';
        $status = 'sent';
        $message = '';
        try{
            $verification = $twilio->verify->v2->services(config('sms.SERVICE_SID'))
                ->verifications
                ->create($args['phone'], "sms");
        }catch(Exception $e){
           $status='error';
           $message = $e->getMessage();
           
        }
        $sid =   $verification != ''?$verification->sid:'';
        \App\PhoneOtp::create([
            'SID' => $sid,
            'phone' => $args['phone'],
            'status' => $status,
            'message' => $message
        ]);
        
       
        return $sid??$message;

    }
    public function signup($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $validator = Validator::make($args, [
            'type' => ['string', 'required', 'in:phone'],
            'phone' => ['required_if:type,phone,', 'string', 'unique:auth_users'],
            'password' => ['required_if:type,phone', 'string', 'min:8'],
        ]);

        if ($validator->fails()) {
            Log::channel('authentication')->info("RegisterMutator (signup): {$validator->messages()->first()}");
            throw new LightHouseCustomException(403, "{$validator->messages()->first()}");
        }

        if($args['type']=='phone' &&  !Auth::guard(config("authentication.guards")[$args['type']])->validateOtp($args)){
            
            throw new LightHouseCustomException(403, "Invalid OTP");
        }

        $user = array(
            'type' => $args['type'],
            'phone' => $args['phone'],
            'password' => Hash::make($args['password']),
            'token' => null,
            'role' => $args['role']
        );

        $user = AuthUser::create($user);

        event(new PostRegisterEvent($user, $args['input']));

        Log::channel('authentication')->info("RegisterMutator (signup): resolver has finished");
        return "User has been created Successfully";
    }

    public function login($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $customMessages = [
            'phone.exists' => __('wrong_creds')
        ];

        $validator = Validator::make($args, [
            'type' => ['string', 'required', 'in:phone, google'],
            'phone' => ['required_if:type,phone,', 'string', 'exists:auth_users'],
            'password' => ['required_if:type,email', 'string', 'min:8'],
            'token' => ['required_if:type,google', 'string'],
        ], $customMessages);

        if ($validator->fails()) {
            Log::channel('authentication')->info("RegisterMutator (login): {$validator->messages()->first()}");
            throw new LightHouseCustomException(403, "{$validator->messages()->first()}");
        }

        $user = Auth::guard(config("authentication.guards")[$args['type']])->attempt($args);

        // if($user->role != $args['role']){
        //     throw new LightHouseCustomException(403, __('wrong_creds'));  
        // }
        switch($args['type']){
            case "phone": 
                if(isset($args['token']) && $args['token']){
                    $user->update(['token'=>$args['token']]);
                }
                break;

            case "google": 
                if(!$user == null){
                    Log::channel('authentication')->info("RegisterMutator (login): Social User logging for the first time with role: ".$args['role']);
                    $user = AuthUser::create([
                        'type' => $args['type'],
                        'role' =>$role,
                        'token' => $args['token'],
                    ]);
                }
                break;
        }

        $user->last_logged_in = Carbon::now();
        $user->save();

        $user = $this->generateJWTToken($user);
        //$whiteListToken = WhiteListToken::updateOrCreate(['auth_user_id' => $user->id], ['token' => $user->jwtToken]);

        Log::channel('authentication')->info("RegisterMutator (login): resolver has finished");
        return $user;
    }

    public function logout($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $authUser = Auth::guard('UserGuard')->user()->authUser;
        $authUser->update(['fcm_token' => NULL]);

        Log::channel('authentication')->info("RegisterMutator (logout): Auth User {$authUser->id} cleared FCM token");
        
        list($token) = sscanf($context->request->header('Authorization'), 'Bearer %s');
        BlackListToken::create(['token' => $token]);

        Log::channel('authentication')->info("RegisterMutator (logout): deleted whitelist Token");

        Log::channel('authentication')->info("RegisterMutator (logout): resolver has finished");
        return "User logged out successfully";
    }

    public function refreshToken($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $authUser = Auth::guard('UserGuard')->user()->authUser;

        $secretKey = config("authentication.JWT_SECRET");
        $secretKey = base64_decode($secretKey);
        $userTokenHeader = $context->request->header('Authorization');
        list($token) = sscanf($userTokenHeader, 'Bearer %s');

        try {
            JWT::$leeway = 60 * 60 * 24; //24 hour
            $decoded = (array) JWT::decode($token, $secretKey, ['HS512']);
            $decoded['iat'] = time();

            $expire = time() + (30 * 86400); // Adding 86400 seconds (1 Month)
            $decoded['exp'] = $expire;

            $generatedToken = JWT::encode($decoded, $secretKey, 'HS512');
            //$whiteListToken = WhiteListToken::updateOrCreate(['auth_user_id' => $authUser->id], ['token' => $generatedToken]);

            Log::channel('authentication')->info("RegisterMutator (refreshToken): resolver has finished");
            $authUser->jwtToken = $generatedToken;
            $authUser->expire = new Carbon($expire);
            return $authUser;
        } catch (\Throwable $th) {
            Log::channel('authentication')->warning("RegisterMutator (refreshToken): {$th}");
            throw new LightHouseCustomException(410, $th);
        }
    }

    public function resetPassword($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        if(!Auth::guard('PhoneGuard')->validateOtp($args)){
            throw new LightHouseCustomException(403, "Invalid OTP");
        }
        
        $user = AuthUser::where('phone',$args['phone'])->first();
        if (!$user) {
            return "password reset successfully";
        }

        $user->update(['password' => Hash::make($args['password'])]);

        Log::channel('authentication')->info("RegisterMutator (resetPassword): sent reset phone to: {$user->phone}");
        Log::channel('authentication')->info("RegisterMutator (resetPassword): resolver has finished");
        return "password reset successfully";
    }

    public function update($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $validator = Validator::make($args, [
            'fcm_token' => ['string', 'required'],
        ]);

        if ($validator->fails()) {
            Log::channel('authentication')->info("RegisterMutator (update): {$validator->messages()->first()}");
            throw new LightHouseCustomException(403, "{$validator->messages()->first()}");
        }
        
        $authUser = Auth::guard('UserGuard')->user()->authUser;
        if ($authUser) {
            $authUser->update(['fcm_token' => $args['fcm_token']]);
            Log::channel('authentication')->info("RegisterMutator (update): Auth User {$authUser->id} updated FCM token");
        }
        Log::channel('authentication')->info("RegisterMutator (update): resolver has finished");
        return 'FCM Token updated successfully';
    }

    protected function generateJWTToken($user)
    {

        $secretKey = config("authentication.JWT_SECRET");

        // $tokenId = base64_encode(mcrypt_create_iv(32));
        $issuedAt = time();
        $notBefore = $issuedAt + 10; //Adding 10 seconds
        $expire = $notBefore + (30 * 86400); // Adding 86400 seconds (1 Month)
        $serverName = "Sercl server"; // Retrieve the server name from config file

        /*
         * Create the token as an array
         */
        $data = [
            'iat' => $issuedAt, // Issued at: time when the token was generated
            // 'jti' => $tokenId, // Json Token Id: an unique identifier for the token
            'iss' => $serverName, // Issuer
            'nbf' => $notBefore, // Not before
            'exp' => $expire, // Expire
            'data' => $user, // Data related to the signer user
        ];

        /*
         * Extract the key, which is coming from the config file.
         *
         * Best suggestion is the key to be a binary string and
         * store it in encoded in a config file.
         *
         * Can be generated with base64_encode(openssl_random_pseudo_bytes(64));
         *
         * keep it secure! You'll need the exact key to verify the
         * token later.
         */

        $secretKey = base64_decode($secretKey);

        /*
         * Encode the array to a JWT string.
         * Second parameter is the key to encode the token.
         *
         * The output string can be validated at http://jwt.io/
         */
        $jwt = JWT::encode(
            $data, //Data to be encoded in the JWT
            $secretKey, // The signing key
            'HS512' // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
        );
        $user->jwtToken = $jwt;
        $user->expire = new Carbon($expire);
        return $user;
    }
}
