<?php

namespace App\GraphQL\Mutations;

use App\AppSetting;
use Carbon\Carbon;
use \Firebase\JWT\JWT;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class FieldResolvers
{
    public function calculatePrice($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $result = new \stdClass;
        $result->vat = (intval(AppSetting::where('key','vat_percentage')->first()->value)/100) * $args['total'];
        $result->vat_percentage = intval(AppSetting::where('key','vat_percentage')->first()->value);
        $result->delivery = intval(AppSetting::where('key','delivery')->first()->value);
        return $result;
    }

}
