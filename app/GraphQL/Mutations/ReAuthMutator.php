<?php

namespace App\GraphQL\Mutations;

use App\AuthUser;
use App\Exceptions\LightHouseCustomException;
use Carbon\Carbon;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use \Firebase\JWT\JWT;

class ReAuthMutator
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $haveToVerifyEmail = false;

        // if we are here , we successed to check that jwt token is valid in our middleWare
        // now let's update the user depending on type

        $user = $context->request->get('authUser');

        $updatedData;

        if (isset($args["email"])) {

            //Check uniqueness of the email
            $emailValidator = Validator::make($args, [
                'email' => ['required', 'string', 'email', 'max:254'],
                'newPassword' => ['required', 'string', 'min:8'],
            ]);

            if ($emailValidator->fails()) {
                Log::channel('authentication')->info("ReAuthMutator (invoke): {$emailValidator->messages()->first()}");
                throw new LightHouseCustomException(403, "{$emailValidator->messages()->first()}");
            }

            //else
            $updatedData["email"] = $args["email"];

            if ($user["email"] != $args["email"]) {
                // he need to change his email so he has to verify it
                $updatedData["email_verified_at"] = null;
                $haveToVerifyEmail = true;
            }
        }


        if (!Hash::check($args['oldPassword'], $user["password"])) {
            Log::channel('authentication')->info("ReAuthMutator (invoke): Old User password is incorrect");
            throw new LightHouseCustomException(403, __("wrong_old_pass"));
        } 
        
        $updatedData["password"] = Hash::make($args['newPassword']);
    


        $updatedUser = AuthUser::where('type', $user['type'])
            ->where('email', $user['email'])
            ->where('token', $user['token'])
            ->update($updatedData);

        if ($haveToVerifyEmail == true) {
            $verify_mail = config("authentication.verify_emails")[$args['role']];
            $user->notify(new $verify_mail()); //Send Verification Mail
            Log::channel('authentication')->info("RegisterMutator (signup): sent verification email to:{$user->email}");
        }

        //Generate JWT Token for updated User
        $result = $this->generateJWTToken($updatedUser);
        $result['id'] = $user['id'];
        Log::channel('authentication')->info("ReAuthMutator (invoke): reAuth has finished");
        return $result;
    }

    protected function generateJWTToken($data)
    {

        $secretKey = config("authentication.JWT_SECRET");

        // $tokenId = base64_encode(mcrypt_create_iv(32));
        $issuedAt = time();
        $notBefore = $issuedAt + 10; //Adding 10 seconds
        $expire = $notBefore + (30 * 86400); // Adding 86400 seconds (1 Month)
        $serverName = "Sercl server"; // Retrieve the server name from config file

        /*
         * Create the token as an array
         */
        $data = [
            'iat' => $issuedAt, // Issued at: time when the token was generated
            // 'jti' => $tokenId, // Json Token Id: an unique identifier for the token
            'iss' => $serverName, // Issuer
            'nbf' => $notBefore, // Not before
            'exp' => $expire, // Expire
            'data' => $data, // Data related to the signer user
        ];

        /*
         * Extract the key, which is coming from the config file.
         *
         * Best suggestion is the key to be a binary string and
         * store it in encoded in a config file.
         *
         * Can be generated with base64_encode(openssl_random_pseudo_bytes(64));
         *
         * keep it secure! You'll need the exact key to verify the
         * token later.
         */

        $secretKey = base64_decode($secretKey);

        /*
         * Encode the array to a JWT string.
         * Second parameter is the key to encode the token.
         *
         * The output string can be validated at http://jwt.io/
         */
        $jwt = JWT::encode(
            $data, //Data to be encoded in the JWT
            $secretKey, // The signing key
            'HS512' // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
        );

        return ['jwtToken' => $jwt, 'expire' => new Carbon($expire)];
    }
}
