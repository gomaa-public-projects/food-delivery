<?php

namespace App\GraphQL\Mutations;

use App\FoodItem;
use App\Chart;
use App\Order;
use App\Customer;
use Carbon\Carbon;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\DB;

class WidgetsResolver
{
    public function dashboardRevenue($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $chart = Chart::where('key','orders_revenue')->whereDate('created_at', Carbon::today())->first();
        if(!$chart){
            $old_chart = Chart::where('key','orders_revenue')->first();
            if($old_chart){
                $old_chart->delete();
                Chart::where('key','net_revenue')->first()->delete();
                Chart::where('key','delivery_revenue')->first()->delete();
            }

            $chart = $this->createOrdersRevenueChart();
        }

        $chart_net = Chart::where('key','net_revenue')->first();
        $chart_delivery = Chart::where('key','delivery_revenue')->first();
        
        $today = 0;
        $today_net = 0;
        $today_delivery = 0;
        $orders = $this->calculate(['today' => true])->get(); 
        foreach($orders as $order){
            $today += $order->total_price;
            $today_net += $order->total_price - $order->delivery;
            $today_delivery += $order->delivery;
        }

        $lables = $chart->lables;
        $value = $chart->value;
        $lables_net = $chart_net->lables;
        $value_net = $chart_net->value;
        $lables_delivery = $chart_delivery->lables;
        $value_delivery = $chart_delivery->value;
        $yesterday = 0;
        
        $yesterday =  $value[count($value)-2];

        $value[count($value)-1] = number_format($today, 2);
        $chart->value = $value;
        $chart->save();

        $value_net[count($value_net)-1] = $today_net;
        $chart_net->value = $value_net;
        $chart_net->save();

        $value_delivery[count($value_delivery)-1] = $today_delivery;
        $chart_delivery->value = $value_delivery;
        $chart_delivery->save();

        $res = new \stdClass;
        $res->percentage = $this->percentageCalc($today, $yesterday);

        $res->lables = json_encode(array_values($lables));
        $res->values = json_encode(array_values($value));

        $res->lables_net = json_encode(array_values($lables_net));
        $res->values_net = json_encode(array_values($value_net));

        $res->lables_delivery = json_encode(array_values($lables_delivery));
        $res->values_delivery = json_encode(array_values($value_delivery));

        $res->total = 0;
        $res->total_net = 0;
        $res->total_delivery = 0;
        $from = Carbon::now()->subMonths(1);
        $to = Carbon::now();
        $orders = $this->calculate(['from' => $from, 'to'=> $to])->get(); 
        foreach($orders as $order){
            $res->total += $order->total_price;
            $res->total_net += $order->total_price - $order->delivery;
            $res->total_delivery += $order->delivery;
        }

        return $res;
    }

    public function dashboardWeeklyRevenue($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $chart = Chart::where('key','orders_revenue_weekly')->whereDate('created_at', Carbon::today())->first();
        if(!$chart){
            $old_chart = Chart::where('key','orders_revenue_weekly')->first();
            if($old_chart){
                $old_chart->delete();
                Chart::where('key','net_revenue_weekly')->first()->delete();
                Chart::where('key','delivery_revenue_weekly')->first()->delete();
            }

            $chart = $this->createOrdersRevenueWeeklyChart();
        }

        $chart_net = Chart::where('key','net_revenue_weekly')->first();
        $chart_delivery = Chart::where('key','delivery_revenue_weekly')->first();
        
        $today = 0;
        $today_net = 0;
        $today_delivery = 0;
        $orders = $this->calculate(['today' => true])->get(); 
        foreach($orders as $order){
            $today += $order->total_price;
            $today_net += $order->total_price - $order->delivery;
            $today_delivery += $order->delivery;
        }

        $lables = $chart->lables;
        $value = $chart->value;
        $lables_net = $chart_net->lables;
        $value_net = $chart_net->value;
        $lables_delivery = $chart_delivery->lables;
        $value_delivery = $chart_delivery->value;
        
        $value[count($value)-1] = number_format($today, 2);
        $chart->value = $value;
        $chart->save();

        $value_net[count($value_net)-1] = $today_net;
        $chart_net->value = $value_net;
        $chart_net->save();

        $value_delivery[count($value_delivery)-1] = $today_delivery;
        $chart_delivery->value = $value_delivery;
        $chart_delivery->save();

        $res = new \stdClass;

        $res->lables = json_encode(array_values($lables));
        $res->values = json_encode(array_values($value));

        $res->lables_net = json_encode(array_values($lables_net));
        $res->values_net = json_encode(array_values($value_net));

        $res->lables_delivery = json_encode(array_values($lables_delivery));
        $res->values_delivery = json_encode(array_values($value_delivery));

        $res->total = 0;
        $res->total_net = 0;
        $res->total_delivery = 0;
        $from = Carbon::now()->subDays(6);
        $to = Carbon::now();
        $orders = $this->calculate(['from' => $from, 'to'=> $to])->get(); 
        foreach($orders as $order){
            $res->total += $order->total_price;
            $res->total_net += $order->total_price - $order->delivery;
            $res->total_delivery += $order->delivery;
        }

        return $res;
    }

    public function dashboardYearlyRevenue($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $chart = Chart::where('key','orders_revenue_yearly')->whereDate('created_at', Carbon::today())->first();
        
        if(!$chart){
            $old_chart = Chart::where('key','orders_revenue_yearly')->first();
            if($old_chart){
                $old_chart->delete();
                Chart::where('key','net_revenue_yearly')->first()->delete();
                Chart::where('key','delivery_revenue_yearly')->first()->delete();
            }

            $chart = $this->createOrdersRevenueYearlyChart();
        }

        $chart_net = Chart::where('key','net_revenue_yearly')->first();
        $chart_delivery = Chart::where('key','delivery_revenue_yearly')->first();
        
        $day = Carbon::now()->endOfMonth();
        $day_before = Carbon::now()->startOfMonth();

        
        $today = 0;
        $today_net = 0;
        $today_delivery = 0;
        $orders = $this->calculate(['from' => $day_before, 'to'=>$day])->get();
        foreach($orders as $order){
            $today += $order->total_price;
            $today_net += $order->total_price - $order->delivery;
            $today_delivery += $order->delivery;
        }

        $lables = $chart->lables;
        $value = $chart->value;
        $lables_net = $chart_net->lables;
        $value_net = $chart_net->value;
        $lables_delivery = $chart_delivery->lables;
        $value_delivery = $chart_delivery->value;
        
        $value[count($value)-1] = number_format($today, 2);
        $chart->value = $value;
        $chart->save();

        $value_net[count($value_net)-1] = $today_net;
        $chart_net->value = $value_net;
        $chart_net->save();

        $value_delivery[count($value_delivery)-1] = $today_delivery;
        $chart_delivery->value = $value_delivery;
        $chart_delivery->save();

        $res = new \stdClass;

        $res->lables = json_encode(array_values($lables));
        $res->values = json_encode(array_values($value));

        $res->lables_net = json_encode(array_values($lables_net));
        $res->values_net = json_encode(array_values($value_net));

        $res->lables_delivery = json_encode(array_values($lables_delivery));
        $res->values_delivery = json_encode(array_values($value_delivery));

        $res->total = 0;
        $res->total_net = 0;
        $res->total_delivery = 0;
        $from = Carbon::now()->subYears(1);
        $to = Carbon::now();
        $orders = $this->calculate(['from' => $from, 'to'=> $to])->get(); 
        foreach($orders as $order){
            $res->total += $order->total_price;
            $res->total_net += $order->total_price - $order->delivery;
            $res->total_delivery += $order->delivery;
        }

        return $res;
    }

    public function createOrdersRevenueWeeklyChart(){
        $data = [];
        $net_data =[];
        $delivery_data =[];
        for ($i = 0; $i<7; $i++){
            $day = Carbon::now()->subDays(6-$i);
            $total = 0;
            $net = 0;
            $delivery = 0;
            $orders = $this->calculate(['date' => $day])->get();
            foreach($orders as $order){
                $total += $order->total_price;
                $net += $order->total_price - $order->delivery;
                $delivery += $order->delivery;
            }
            $data[$day->format('l')] = number_format($total, 2);
            $net_data[$day->format('l')] = number_format($net, 2);
            $delivery_data[$day->format('l')] = number_format($delivery, 2);
        }
        Chart::create([
            'key' => 'net_revenue_weekly',
            'value' => array_values($net_data),
            'lables' => array_keys($net_data)
        ]);
        Chart::create([
            'key' => 'delivery_revenue_weekly',
            'value' => array_values($delivery_data),
            'lables' => array_keys($delivery_data)
        ]);
        return Chart::create([
            'key' => 'orders_revenue_weekly',
            'value' => array_values($data),
            'lables' => array_keys($data)
        ]);
        
    }

    public function createOrdersRevenueYearlyChart(){
        $data = [];
        $net_data =[];
        $delivery_data =[];
        for ($i = 0; $i<12; $i++){
            $day = Carbon::now()->subMonths(11-$i)->endOfMonth();
            $day_before = Carbon::now()->subMonths(11-$i)->startOfMonth();
            $total = 0;
            $net = 0;
            $delivery = 0;
            
            $orders = $this->calculate(['from' => $day_before, 'to'=>$day])->get();
            
            foreach($orders as $order){
                $total += $order->total_price;
                $net += $order->total_price - $order->delivery;
                $delivery += $order->delivery;
            }

            $data[$day->format('F')] = number_format($total, 2);
            $net_data[$day->format('F')] = number_format($net, 2);
            $delivery_data[$day->format('F')] = number_format($delivery, 2);
        }

        Chart::create([
            'key' => 'net_revenue_yearly',
            'value' => array_values($net_data),
            'lables' => array_keys($net_data)
        ]);
        Chart::create([
            'key' => 'delivery_revenue_yearly',
            'value' => array_values($delivery_data),
            'lables' => array_keys($delivery_data)
        ]);
        return Chart::create([
            'key' => 'orders_revenue_yearly',
            'value' => array_values($data),
            'lables' => array_keys($data)
        ]);
        
    }

    public function dashboardCustomers($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $chart = Chart::where('key','customer_total')->whereDate('created_at', Carbon::today())->first();
        if(!$chart){
            $old_chart = Chart::where('key','customer_total')->first();
            if($old_chart){
                $old_chart->delete();
            }

            $chart = $this->createCustomerTotalChart();
        }

        $lables = $chart->lables;
        $value = $chart->value;
        $today =  $this->calculateCustomers(['today' => true])->count();
        $yesterday =  $this->calculateCustomers(['yesterday' => true])->count();

        $value[count($value)-1] = number_format($today, 2);
        $chart->value = $value;
        $chart->save();

        $res = new \stdClass;
        $res->percentage = $this->percentageCalc($today, $yesterday);

        $res->lables = json_encode(array_values($lables));
        $res->values = json_encode(array_values($value));
        $res->total = $this->calculateCustomers([])->count();

        return $res;
    }
    public function dashboardOrders($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $chart = Chart::where('key','orders_total')->whereDate('created_at', Carbon::today())->first();
        if(!$chart){
            $old_chart = Chart::where('key','orders_total')->first();
            if($old_chart){
                $old_chart->delete();
            }

            $chart = $this->createOrdersTotalChart();
        }

        $lables = $chart->lables;
        $value = $chart->value;
        $today =  $this->calculate(['today' => true])->count();
        $yesterday =  $this->calculate(['yesterday' => true])->count();

        $value[count($value)-1] = number_format($today, 2);
        $chart->value = $value;
        $chart->save();

        $res = new \stdClass;
        $res->percentage = $this->percentageCalc($today, $yesterday);

        $res->lables = json_encode(array_values($lables));
        $res->values = json_encode(array_values($value));
        $res->total = $this->calculate([])->count();

        return $res;
    }
    public function calculate($args){
        $orders = Order::whereNotIn('status',['archived']);

        if(isset($args['branch_id'])){
            $orders->where('branch_id',$args['branch_id']);
        }
        if(isset($args['driver_id'])){
            $orders->where('driver_id',$args['driver_id']);
        }
        if(isset($args['today']) && $args['today']){
            $orders->whereDate('created_at','=',Carbon::now()->toDateString());
        }
        else if(isset($args['yesterday']) && $args['yesterday']){
            $orders->whereDate('created_at','>=',Carbon::now()->subDays(1)->toDateString())->whereDate('created_at','<=',Carbon::now()->subDays(1)->toDateString());
        }
        else if(isset($args['weekly']) && $args['weekly']){
            $orders->whereDate('created_at','>=',Carbon::now()->subDays(7)->toDateString())->whereDate('created_at','<=',Carbon::now()->toDateString());
        }
        else if(isset($args['monthly']) && $args['monthly']){
            $orders->whereDate('created_at','>=',Carbon::now()->subDays(30)->toDateString())->whereDate('created_at','<=',Carbon::now()->toDateString()); 
        }
        else if(isset($args['from']) && isset($args['to'])){
            $orders->whereDate('created_at','>=',$args['from']->toDateString())->whereDate('created_at','<=',$args['to']->toDateString()); 
        }
        else if(isset($args['from'])){
            $orders->whereDate('created_at','>=',$args['from']->toDateString()); 
        }
        else if(isset($args['to'])){
            $orders->whereDate('created_at','<=',$args['to']->toDateString()); 
        }
        else if(isset($args['date'])){
            $orders->whereDate('created_at','=',$args['date']->toDateString()); 
        }
        return $orders;
    }
    public function calculateCustomers($args){
        $customers = Customer::whereNotNull('id');

        if(isset($args['today']) && $args['today']){
            $customers->whereDate('created_at','=',Carbon::now()->toDateString());
        }
        else if(isset($args['yesterday']) && $args['yesterday']){
            $customers->whereDate('created_at','>=',Carbon::now()->subDays(1)->toDateString())->whereDate('created_at','<=',Carbon::now()->subDays(1)->toDateString());
        }
        else if(isset($args['weekly']) && $args['weekly']){
            $customers->whereDate('created_at','>=',Carbon::now()->subDays(7)->toDateString())->whereDate('created_at','<=',Carbon::now()->toDateString());
        }
        else if(isset($args['monthly']) && $args['monthly']){
            $customers->whereDate('created_at','>=',Carbon::now()->subDays(30)->toDateString())->whereDate('created_at','<=',Carbon::now()->toDateString()); 
        }
        else if(isset($args['from']) && isset($args['to'])){
            $customers->whereDate('created_at','>=',$args['from']->toDateString())->whereDate('created_at','<=',$args['to']->toDateString()); 
        }
        else if(isset($args['from'])){
            $customers->whereDate('created_at','>=',$args['from']->toDateString()); 
        }
        else if(isset($args['to'])){
            $customers->whereDate('created_at','<=',$args['to']->toDateString()); 
        }
        else if(isset($args['date'])){
            $customers->whereDate('created_at','=',$args['date']->toDateString()); 
        }
        return $customers;
    }

    public function percentageCalc($today, $yesterday){
        $today = floatval($today);
        $yesterday = floatval($yesterday);

        $diff = $today - $yesterday;

        $sign = '<span class="text-success fs-14">+';
        $percentage = 0;

        if($diff > 0) {
            $percentage = ($diff/ $today) * 100;
        }
        if($diff < 0) {
            $sign ='<span class="text-danger fs-14">-';
            $percentage = ($diff/ $yesterday) * 100 * -1;
        }
        
        return $sign.number_format($percentage, 1)."%</span>";
    }

    public function createOrdersTotalChart(){
        for ($i = 0; $i<30; $i++){
            $day = Carbon::now()->subDays(29-$i);
            $data[$day->format('m-d')] = $this->calculate(['date' => $day])->count();
        }
        return Chart::create([
            'key' => 'orders_total',
            'value' => array_values($data),
            'lables' => array_keys($data)
        ]);
    }
    public function createOrdersRevenueChart(){
        $data = [];
        $net_data =[];
        $delivery_data =[];
        for ($i = 0; $i<30; $i++){
            $day = Carbon::now()->subDays(29-$i);
            $total = 0;
            $net = 0;
            $delivery = 0;
            $orders = $this->calculate(['date' => $day])->get();
            foreach($orders as $order){
                $total += $order->total_price;
                $net += $order->total_price - $order->delivery;
                $delivery += $order->delivery;
            }
            $data[$day->format('m-d')] = number_format($total, 2);
            $net_data[$day->format('m-d')] = number_format($net, 2);
            $delivery_data[$day->format('m-d')] = number_format($delivery, 2);
        }
        Chart::create([
            'key' => 'net_revenue',
            'value' => array_values($net_data),
            'lables' => array_keys($net_data)
        ]);
        Chart::create([
            'key' => 'delivery_revenue',
            'value' => array_values($delivery_data),
            'lables' => array_keys($delivery_data)
        ]);
        return Chart::create([
            'key' => 'orders_revenue',
            'value' => array_values($data),
            'lables' => array_keys($data)
        ]);
        
    }
    public function createCustomerTotalChart(){
        for ($i = 0; $i<30; $i++){
            $day = Carbon::now()->subDays(29-$i);
            $data[$day->format('m-d')] = $this->calculateCustomers(['date' => $day])->count();
        }
        return Chart::create([
            'key' => 'customer_total',
            'value' => array_values($data),
            'lables' => array_keys($data)
        ]);
    }

    public function orders($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $res = new \stdClass;
        $res->orders = $this->calculate($args)->orderBy('created_at','desc')->limit(10)->get();
        return $res;
    }

    public function popularFood($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $list = [];
        $order_items = DB::table('order_items')->select('food_item_id'  ,DB::raw('count(*) as total'))
        ->groupBy('food_item_id')->orderBy('total','DESC')
        ->get();

        $rank = 1;
        foreach($order_items as $order_item){
            $list_item = new \stdClass;
            $list_item->count = $order_item->total;
            $list_item->foodItem = FoodItem::withTrashed()->find($order_item->food_item_id);
            $list_item->rank = $rank;
            $list[] = $list_item;
            $rank += 1;
        }

        return $list;
    }

    // Old Queries
    public function orders_dashboard($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $total = 0;
        $res = new \stdClass;
        $res->count = $this->calculate($args)->count();
        $orders = $this->calculate($args)->get();
        foreach($orders as $order){
            $total += $order->total_price - $order->delivery;
        }
        $res->total = $total;
        $res->orders = $this->calculate($args)->orderBy('created_at','desc')->limit(10)->get();
        return $res;
    }
    public function orders_driver($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $total = 0;
        $res = new \stdClass;
        $res->count = $this->calculate($args)->count();
        $orders = $this->calculate($args)->get();
        foreach($orders as $order){
            $total += $order->delivery;
        }
        $res->total = $total;
        return $res;
    }
    public function customers($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        return Customer::count();
    }
    public function profits($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $total = 0;
        $orders = Order::where('status','delivered')->get();
        foreach($orders as $order){
            $total += $order->total_price;
        }

        return $total;
    }
}
