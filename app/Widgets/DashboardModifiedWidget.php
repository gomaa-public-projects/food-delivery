<?php

namespace App\Widgets;

use Illuminate\Support\Facades\Auth;
use Akwad\VoyagerExtension\Widgets\BaseDimmer;
use Akwad\VoyagerExtension\Models\Role;
use Carbon\Carbon;

class DashboardModifiedWidget extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [], $allowed_roles = ['admin','user'];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('dashboardModified');
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        $role_id = Auth::guard('MyVoyagerGuard')->user()->role_id;
        $name = Role::find($role_id)->name;
        return (in_array($name, $this->allowed_roles));
    }
}
