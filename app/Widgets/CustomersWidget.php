<?php

namespace App\Widgets;

use Illuminate\Support\Facades\Auth;
use Akwad\VoyagerExtension\Facades\Voyager;
use Akwad\VoyagerExtension\Widgets\BaseDimmer;
use Akwad\VoyagerExtension\Models\Role;
use Carbon\Carbon;

class CustomersWidget extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [], $allowed_roles = ['admin', 'user'];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $image_link="customer.jpg";
        return view('widgets.CustomersWidget', array_merge($this->config, [
            'icon'   => 'voyager-person',
            'image' => asset('widgets/'.$image_link),
            'refresh' => asset('widgets/refresh.png'),
            'refresh_active' => asset('widgets/refresh_active.png'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        $role_id = Auth::guard('MyVoyagerGuard')->user()->role_id;
        $name = Role::find($role_id)->name;
        return (in_array($name, $this->allowed_roles));
    }
}
