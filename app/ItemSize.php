<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ItemSize extends Model
{
    protected $fillable = [
        'price',
        'name',
        'food_item_id',
    ];
    public function foodItem(): BelongsTo
    {
        return $this->belongsTo(FoodItem::class)->withTrashed();
    }

}
