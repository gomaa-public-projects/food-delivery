<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PostRegisterEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $auth_user;
    public $extras;

    public function __construct($auth_user, $extras)
    {
        $this->auth_user = $auth_user;
        $this->extras = $extras;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('auth-channel');
    }
}
