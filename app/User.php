<?php

namespace App;

use Akwad\VoyagerExtension\Facades\Voyager;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends \Akwad\VoyagerExtension\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeMyProfile($query)
    {
        return $query->where('id', '=', Auth()->user()->id);
    }

    public function authUser():BelongsTo
    {
        return $this->belongsTo(AuthUser::class);
    }
    public function getRoleIdAttribute($value)
    {
        return Voyager::modelClass('Role')::where('name',$this->authUser->role)->first()->id;
    }
}
