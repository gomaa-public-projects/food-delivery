<?php

namespace App;

use App\Traits\TimeZone;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
class Order extends Model
{
    use TimeZone;
    protected $fillable = [
        'branch_id',
        'customer_id',
        'notes',
        'status',
        'price',
        'delivery',
        'vat',
        'payment_method',
        'payment_status',
        'order_node_id',
        'reason',
        'invitation_time',
        'payment_reference',
        'payment_transaction_id',
        'authorization_code',
    ];

    public function getAddressDescriptionAttribute(){
        if($this->customer && $this->customer->address){
            return $this->customer->address->description;
        }
    }
    public function branch(): BelongsTo
    {
        return $this->belongsTo(Branch::class);
    }
    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }
    public function driver(): BelongsTo
    {
        return $this->belongsTo(Driver::class);
    }
    public function orderItems(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }
    public function rejectedInvitations(): HasMany
    {
        return $this->hasMany(RejectedInvitation::class);
    }
    public function getPriceAttribute(){
        $price = 0;
        if($this->orderItems != []){
            $price = $this->orderItems->sum('price');
        }
        return $price;
    }
    public function getPaymentMethodAttribute($value){
        $available_payment_options = [
            'VISA',
            'MASTERCARD',
            'MADA',
        ]; 
        if(!$value || !in_array($value, $available_payment_options)){
            return null;
        }
        return $value;
    }
    public function getVatAttribute(){
        return (intval(AppSetting::where('key','vat_percentage')->first()->value)/100) * $this->price;
    }
    public function getDeliveryAttribute(){
        return intval(AppSetting::where('key','delivery')->first()->value);
    }
    public function getRangeAttribute(){
        return floatval(AppSetting::where('key','driver_branch_arrival')->first()->value);
    }
    public function getTotalPriceAttribute(){
        return $this->price + $this->vat + $this->delivery;
    }
    public function setReasonAttribute($value){
        if(!$value || $this->status != 'rejected'){
            return;
        }
        $this->attributes['reason'] = $value;
    }
    public function estimatedMinutes(){
        return intval(AppSetting::where('key','estimated_order_minutes')->first()->value);
    }
    
    public function getCreatedAtAttribute($value){
        return $this->shiftDate($value);
    }
}
