<?php
namespace App\Traits;

use Kreait\Firebase;
use Kreait\Firebase\Factory;

trait LocationTrait
{
    public $database;

    public function __construct()
    {
        $firebase = (new Factory)
            ->withDatabaseUri(config('firebase.FBDATABASE_URL'));
        $this->database = $firebase->createDatabase();       
    }

    function getLocation()
    {
        $location = new \stdClass;
        $location->longitude = 0;
        $location->latitude = 0;

        $ref = "drivers/$this->driver_node_id";
        $oldMetaData = $this->database->getReference($ref)->getValue();

        if(!$oldMetaData){
            return $location;
        }
        $location->longitude = floatval($oldMetaData["meta-data"]["longitude"]);
        $location->latitude = floatval($oldMetaData["meta-data"]["latitude"]);
        return $location;
    }
}