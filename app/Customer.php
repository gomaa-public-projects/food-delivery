<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Customer extends \Akwad\VoyagerExtension\Models\User
{
    protected $fillable = [
        "username",
        "photo",
        "phone",
        "auth_user_id",
        "customer_token",
        "card_number",
        "payment_option",
        "email",
    ];
    
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }
    public function customerNotifications(): HasMany
    {
        return $this->hasMany(CustomerNotification::class);
    }
    public function authUser(): BelongsTo
    {
        return $this->belongsTo(AuthUser::class);
    }
    public function address(): HasOne
    {
        return $this->hasOne(Address::class,'customer_id', 'id');
    }
    public function getPhotoAttribute($value)
    {
        if (!request()->is('graphql'))
        {
            return $value;
        }
       
        if ($value == null) {return $value;}
        return asset('storage/' . $value);
    }
    public function getPhoneAttribute(){
        if(isset($this->authUser)){
            return $this->authUser->phone;
        }
        return NULL;
    }
    public function getHasPaymentInfoAttribute(){
        return $this->customer_token? true: false;
    }
    public function getCardNumberAttribute($value){
        return str_replace("*","",$value);
    }
}
