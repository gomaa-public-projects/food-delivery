<?php

namespace App\MobileNotifications\Traits;

use Illuminate\Support\Facades\App;
use App\NotificationIcon;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;
use Illuminate\Support\Facades\Log;


trait NotificationTrait
{
    public $id, $connection, $delay, 
    $name = 'DefaultNotification',
    $title = 'title text',
    $description = 'description text',
    $route = "/customer-home",
    $args = null,
    $icon = "default",
    $notification_title_data = [],
    $notification_body_data = [],
    $notification_text_type = "",
    $locale,
    $data= [],
    $additional_data = [];

    public function via($notifiable)
    {
        return [FcmChannel::class];
    }

    public function toFcm($notifiable)
    {
        $this->locale = App::getLocale();
        if($notifiable && $notifiable->locale){
            $this->locale = $notifiable->locale;
        }

        $this->title =  __("fcm_notifications.title.$this->notification_text_type", $this->notification_title_data, $this->locale);
        $this->description =  __("fcm_notifications.body.$this->notification_text_type", $this->notification_body_data, $this->locale);

        $this->createData();
        $this->name = str_replace('App\MobileNotifications\\','',get_class($this));
        
        Log::channel('notifications')->info("{$this->name}: notification started");

        $fcmNotification = FcmNotification::create()
            ->setTitle($this->title)
            ->setBody($this->description);
            
        Log::channel('notifications')->info("{$this->name}: Auth_User id is: ".$notifiable->id);
        Log::channel('notifications')->info("{$this->name}: fcm token is: ".$notifiable->fcm_token);
        Log::channel('notifications')->info("{$this->name}: title: ".$this->title);
        Log::channel('notifications')->info("{$this->name}: description: ".$this->description);
        Log::channel('notifications')->info("{$this->name}: data: ".json_encode($this->data));
        return FcmMessage::create()
            ->setNotification($fcmNotification)
            ->setData($this->data);
    }

    public function handleIcon(){
        $icon = NotificationIcon::where('notification_title',$this->icon)->first();
        if(!$icon){
            $icon = NotificationIcon::where('notification_title','default')->first();
        }
        return asset('storage/' . $icon->icon_link);
    }
    public function createData(){
        $this->data["click_action"] = 'FLUTTER_NOTIFICATION_CLICK';
        $this->data["route"] = $this->route;
        $this->data["args"] = $this->args;
        $this->data["icon"] = $this->handleIcon();
        $this->data["additional_data"] = json_encode($this->additional_data);
        $this->data["content_available"] = 'true';
    }
}
