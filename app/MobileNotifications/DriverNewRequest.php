<?php

namespace App\MobileNotifications;

use Illuminate\Notifications\Notification;
use App\MobileNotifications\Traits\NotificationTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DriverNewRequest extends Notification implements ShouldQueue
{
    use InteractsWithQueue ,NotificationTrait;

    public $queue = 'notifications';

    public function __construct()
    {
        $this->notification_text_type = "driver_new_request";
        $this->args = null;
        $this->route = "/home-screen";
        $this->icon ='driver_new_request';
    }
}
