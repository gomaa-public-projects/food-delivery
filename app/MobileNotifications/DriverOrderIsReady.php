<?php

namespace App\MobileNotifications;

use Illuminate\Notifications\Notification;
use App\MobileNotifications\Traits\NotificationTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DriverOrderIsReady extends Notification implements ShouldQueue
{
    use InteractsWithQueue ,NotificationTrait;

    public $queue = 'notifications';

    public function __construct()
    {
        $this->notification_text_type = "driver_order_is_ready";
        $this->args = null;
        $this->route = "/home-screen";
        $this->icon ='driver_order_is_ready';
    }
}
