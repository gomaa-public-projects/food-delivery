<?php

namespace App\MobileNotifications;

use Illuminate\Notifications\Notification;
use App\MobileNotifications\Traits\NotificationTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\CustomerNotification;

class CustomerOrderIsReady extends Notification implements ShouldQueue
{
    use InteractsWithQueue ,NotificationTrait;

    public $queue = 'notifications';

    public function __construct($customer_id)
    {
        $this->notification_text_type = "customer_order_is_ready";
        $this->args = null;
        $this->route = "/home-screen";
        $this->icon ='customer_order_is_ready';

        CustomerNotification::create([
            "title" => $this->title,
            "description" => $this->description,
            "route" => $this->route,
            "customer_id" => $customer_id,
        ]);
    }
}
