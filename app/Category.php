<?php

namespace App;

use Exception;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'photo',
        'from',
        'to',
        'serve_all_day',
    ];

    public function getPhotoAttribute($value)
    {
        if (!request()->is('graphql'))
        {
            return $value;
        }
       
        if ($value == null) {return $value;}
        return asset('storage/' . $value);
    }
    public function foodItems(): HasMany
    {
        return $this->hasMany(FoodItem::class);
    }

    public function getFromAttribute($value){
        if(!$value){
            return $value;
        }
        return Carbon::createFromFormat('Y-m-d H:i:s',$value,'UTC')->setTimezone(config("app.TIMEZONE"))->format('H:i:s');
    }
    public function getToAttribute($value){
        if(!$value){
            return $value;
        }
        return Carbon::createFromFormat('Y-m-d H:i:s',$value,'UTC')->setTimezone(config("app.TIMEZONE"))->format('H:i:s');
    }
    public function setFromAttribute($value){
        if(!$value){
            return;
        }
        try{
            $this->attributes['from'] = Carbon::createFromFormat('H:i:s',$value,config("app.TIMEZONE"))->setTimezone("UTC")->format('Y-m-d H:i:s');
        }
        catch(Exception $e){
            $this->attributes['from'] = Carbon::createFromFormat('H:i:s',$value.":00",config("app.TIMEZONE"))->setTimezone("UTC")->format('Y-m-d H:i:s');
        }
    }
    public function setToAttribute($value){
        if(!$value){
            return;
        }
        try{
            $this->attributes['to'] = Carbon::createFromFormat('H:i:s',$value,config("app.TIMEZONE"))->setTimezone("UTC")->format('Y-m-d H:i:s');
        }
        catch(Exception $e){
            $this->attributes['to'] = Carbon::createFromFormat('H:i:s',$value.":00",config("app.TIMEZONE"))->setTimezone("UTC")->format('Y-m-d H:i:s');
        }
    }
}
