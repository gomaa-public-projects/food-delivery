<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chart extends Model
{
    protected $fillable = [
        'key',
        'value',
        'lables',
    ];

    public function getValueAttribute($value){
        return json_decode($value);
    }
    public function setValueAttribute($value){
        $this->attributes['value'] = json_encode(array_values($value));
    }
    public function getLablesAttribute($value){
        return json_decode($value);
    }
    public function setLablesAttribute($value){
        $this->attributes['lables'] = json_encode(array_values($value));
    }
}
