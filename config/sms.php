<?php

return [
    "ACCOUNT_SID" => env("TWILIO_ACCOUNT_SID"),
    "AUTH_TOKEN" => env("TWILIO_AUTH_TOKEN"),
    "SERVICE_SID" => env("TWILIO_SERVICE_SID"),
];