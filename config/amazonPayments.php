<?php

return [
    "ACCESS_CODE" => env("ACCESS_CODE"),
    "MERCHANT_ID" => env("MERCHANT_ID"),
    "REQUEST_PHRASE" => env("REQUEST_PHRASE"),
    "CURRENCY" => "SAR",
    "SANDBOX" => env("SANDBOX"),
    "LANGUAGE" => "en"
];
