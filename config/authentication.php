<?php

return [
    'verify_emails' => [
        'customer' => "App\Notifications\VerifyCustomer",
    ],
    'reset_emails' => [
        'customer' => "App\Notifications\ResetCustomerPassword",
    ],

    "JWT_SECRET" => env("JWT_SECRET"),

    'guards' => [
        'phone' => 'PhoneGuard',
        'google' => 'SocialGuard',
        'facebook' => 'SocialGuard',
    ],
];
