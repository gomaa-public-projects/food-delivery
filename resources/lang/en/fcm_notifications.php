<?php

return [

    "title" => [
        "driver_new_request" => "New Order",
        "customer_order_is_ready" => "Your order is ready",
        "customer_order_on_his_way" => "Order on the way",
        "customer_order_rejected" => "Order Rejected",
        "customer_order_delieverd" => "Order Deliverd",
        "driver_order_is_ready" => "Order is ready"
    ],
    "body" => [
        "driver_new_request" => "You have a new order",
        "customer_order_is_ready" => "Driver will pick up your order soon",
        "customer_order_on_his_way" => "Your Order Is on the way to you",
        "customer_order_rejected" => "We are very sorry",
        "customer_order_delieverd" => "Thank you for ordering",
        "driver_order_is_ready" => "Pick up your order from the restaurant"
    ]

];
