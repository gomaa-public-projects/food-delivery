<?php

return [

    "title" => [
        "driver_new_request" => "طلب جديد",
        "customer_order_is_ready" => "الطلب جاهز",
        "customer_order_on_his_way" => "الطلب في الطريق",
        "customer_order_delieverd" => "تم توصيل الطلب",
        "customer_order_rejected" => "تم رفض الطلب",
        "driver_order_is_ready" => "الطلب جاهز"
    ],
    "body" => [
        "driver_new_request" => "لديك طلب جديد",
        "customer_order_is_ready" => "الطلب جاهز للإستلام",
        "customer_order_on_his_way" => "الطلب في الطريق إليك",
        "customer_order_delieverd" => "شكرا لإستخدامك خدماتنا",
        "customer_order_rejected" => "عذرا، سنحاول حل المشكلة",
        "driver_order_is_ready" => "يمكنك إستلام الطلب من المطعم"
    ]

];
