<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Food Delivery</title>

        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ asset('css/payment.css') }}" rel="stylesheet">


    </head>
    <body>
            <div class="content">
                <div class="m-b-md" id="success" style="display:none; margin-top:35vh">
                    <h1>جاري تحويلك لمتابعة الطلب</h1>
                </div>
                <div class="m-b-md" id="error" style="display:none; margin-top:35vh">
                    <h1 class="innerTitle">عفوا! حدث خطأ ما</h1>
                    <div class="paymentErrors" id="paymentErrors">
                    </div>
                </div>
            </div>
            <script>
                const queryString = window.location.search;
                const urlParams = new URLSearchParams(queryString);
                const success = urlParams.get('success');
                const error = urlParams.get('error');
                const message = urlParams.get('message')
                if(success == "true"){
                    document.getElementById('success').style.display="block";
                }
                else{
                    if(error) {
                        document.getElementById('paymentErrors').innerHTML+="<span class=\"errorHead\"></span>"+error;
                        }
                    
                    if(message) {
                        document.getElementById('paymentErrors').innerHTML+="<span class=\"errorHead\"></span>"+message;
                        }
                        
                    document.getElementById('error').style.display="block";
                }

                function goBack() {
                    window.history.back();
                }
            </script>
    </body>
</html>
