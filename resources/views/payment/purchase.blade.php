<!DOCTYPE html>
<html>
    <head></head>
    <body>
        <div class="content" style="text-align:center">
        <h1>Redirecting ...</h1>
         <form name="purchase" method="POST" action="{{$action}}">
            @foreach($data as $key => $value)
                <input  name="{{$key}}" value="{{$value}}" type ='hidden'>
            @endforeach
        </form> 
        </div>
        <script>
            document.purchase.submit();
        </script>
    </body>
</html>
