<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />

        <!-- Include Bootstrap Datepicker -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

    </head>
    <body>
            @php
                $branches = App\Branch::all();
                $drivers = App\Driver::all();
                $customers = App\Customer::count();
            @endphp
        <div class="main" id="main">
            <div class="table-wrapper">
                    <table class="fl-table">
                        <thead>
                            <tr>
                                <th>Branch</th>
                                <th>From</th>
                                <th>To</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <select name="branches" class="form-control" id="branches">
                                             @foreach ($branches as $branch)
                                                <option value="{{$branch->id}}">{{$branch->name}}</option>
                                            @endforeach
                                    </select>
                                    
                                </td>
                                <td>
                                    <input type="text" class="form-control" id="datepicker_from">
                                </td>
                                <td>
                                        <input type="text" class="form-control" id="datepicker_to">
                                </td>
                            </tr>

                            <tr>
                                <td colspan="1">
                                    <span style="margin-right:5%;">Select all Branches</span> <input type="checkbox" id="branches_check">
                                </td>
                                <td colspan ="2">
                                    <input type="radio" id="today" name="period" value="today">
                                    <label for="today">Today</label>
                                    <input type="radio" id="weekly" name="period" value="weekly">
                                    <label for="weekly">Weekly</label>
                                    <input type="radio" id="monthly" name="period" value="monthly">
                                    <label for="monthly">Monthly</label>
                                    <input type="radio" id="custom" name="period" value="custom">
                                    <label for="custom">Custom</label>

                                    <input type="hidden" id="radio" value="custom">
                                </td>
                            </tr>
                    <tbody>
                </table>
                <div class="parent">
                    <div class="child child1">
                        <p class ="orders_res_head">Total Orders: </p>
                        <p class ="orders_res" id="total_orders"></p>
                    </div>
                    <div class="child child2">
                        <p class ="orders_res_head">Total Profits: </p>
                        <p class ="orders_res" id="total_profits"></p>
                    </div>
                </div>
            </div>
        </div>


        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="main row clearfix" id="main_mini_orders" style="display:none;">
                    
                </div>
            </div>
        </div>



        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="main" id="main_drivers">
                    <div class="table-wrapper">
                            <table class="fl-table">
                                <thead>
                                    <tr>
                                        <th>Driver</th>
                                        <th>From</th>
                                        <th>To</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <select name="drivers" class="form-control" id="drivers">
                                                    @foreach ($drivers as $driver)
                                                        <option value="{{$driver->id}}">#{{$driver->id}}{{": ".$driver->name}}</option>
                                                    @endforeach
                                            </select>
                                            
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="datepicker_from_driver">
                                        </td>
                                        <td>
                                                <input type="text" class="form-control" id="datepicker_to_driver">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="1">
                                            <span style="margin-right:5%;">Select all Drivers</span> <input type="checkbox" id="drivers_check">
                                        </td>
                                        <td colspan ="2">
                                            <input type="radio" id="today_driver" name="period_driver" value="today">
                                            <label for="today_driver">Today</label>
                                            <input type="radio" id="weekly_driver" name="period_driver" value="weekly">
                                            <label for="weekly_driver">Weekly</label>
                                            <input type="radio" id="monthly_driver" name="period_driver" value="monthly">
                                            <label for="monthly_driver">Monthly</label>
                                            <input type="radio" id="custom_driver" name="period_driver" value="custom">
                                            <label for="custom_driver">Custom</label>

                                            <input type="hidden" id="radio_driver" value="custom">
                                        </td>
                                    </tr>
                            <tbody>
                        </table>
                        <div class="parent_mini">
                            <div class="child child1">
                                <p class ="orders_res_head_mini">Total Orders: </p>
                                <p class ="orders_res_mini" id="total_orders_driver"></p>
                            </div>
                            <div class="child child2">
                                <p class ="orders_res_head_mini">Total Delivery: </p>
                                <p class ="orders_res_mini" id="total_profits_driver"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="display:none;">
                <div class="main">
                <div class="table-wrapper">
                    <div class="parent_mini_customers">
                        <p class ="orders_res">Total Customers: </p>
                        <p class ="orders_res">{{$customers}}</p>
                    </div>
                </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('js/branches.js') }}"></script>
        <script src="{{ asset('js/drivers.js') }}"></script>
    </body>
</html>