<div class="panel widget center bgimage" 
     style="margin-bottom:0;overflow:hidden;background-image:url('{{ $image }}');min-height:395px;">
    <div class="dimmer"></div>
    <div class="panel-content">
        @if (isset($icon))<i class='{{ $icon }}'></i>@endif
        <h4 id='customers_title'></h4>
        <p id='customers_text'></p>
        <div id="customers_refresh" 
            onclick="callCustomersGraphQL()"
            style="display:block; background: url({{$refresh}});max-width:20px;min-height:20px;background-size:cover"
            onmouseover="customers_mouseIn()"
            onmouseout="customers_mouseOut()"
        ></div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    function customers_mouseIn(){
        var refresh_button=document.getElementById("customers_refresh");
        refresh_button.style.backgroundImage = "url({{$refresh_active}})";
    };
    function customers_mouseOut(){
        var refresh_button=document.getElementById("customers_refresh");
        refresh_button.style.backgroundImage = "url({{$refresh}})";
    };
    window.onload = callCustomersGraphQL();
    function callCustomersGraphQL()
    {
        $.ajax({
            type: 'POST', 
            url: '/graphql',
            data:{query:'query{customers}'},
            success: function (data) {
                console.log(data);
                var customers = data.data.customers;
                if(customers == 1){
                    $("#customers_title").html(customers+" Customer");
                    $("#customers_text").html("You have "+customers+" Customer.");
                }
                else{
                    $("#customers_title").html(customers+" Customers");
                    $("#customers_text").html("You have "+customers+" Customers.");
                }
            },
            error: function() { 
                console.log(data);
            }
        });
    };
</script>
