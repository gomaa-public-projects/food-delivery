<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="{{ asset('css/my_style.css') }}">
    </head>
    <body>
        <script>
            var current_waiting = new Array();
            var current_preparing = new Array();
            var current_ready = new Array();
        </script>

        <div id="waiting_branch" ondrop="drop(event)" ondragover="allowDrop(event)" name="new" acceptDrop = "true">
            <p style="font-size:larger">New</p>
            @php
                $orders = App\Order::where('branch_id',$user)->where('status','waiting_branch')->get();
            @endphp

            @foreach($orders as $order)
                <div   draggable="true" ondragstart="drag(event)" id="drag{{1024+$order->id}}" class="movable movred" order_id="{{$order->id}}" order="true" status="pending"> 
                    <span order_id="{{$order->id}}" order="true" class="highlights" style="display:block;">{{$order->id}}</span>
                    Order Summary <span order_id="{{$order->id}}" order="true" class="highlights" style="display:inline-block;">{{$order->price}} SAR</span>
                    <div order_id="{{$order->id}}" order="true" style="display:block" id="status_parent{{$order->id}}">
                        <div order_id="{{$order->id}}" order="true" id="status{{$order->id}}" class="status statusR"></div><span id="status_txt_{{$order->id}}">pending</span>
                    </div>
                </div>
                <script>current_waiting.push(parseInt({{$order->id}}))</script>
            @endforeach
            
        </div>
        <div id="preparing" ondrop="drop(event)" ondragover="allowDrop(event)" name="preparing" acceptDrop = "true">
            <p style="font-size:larger">Preparing</p>

            @php
                $orders = App\Order::where('branch_id',$user)->where('status','preparing')->get();
            @endphp

            @foreach($orders as $order)
                <div   draggable="true" ondragstart="drag(event)" id="drag{{1024+$order->id}}" class="movable movblue" order_id="{{$order->id}}" order="true" status="preparing"> 
                    <span order_id="{{$order->id}}" order="true" class="highlights" style="display:block;">{{$order->id}}</span>
                    Order Summary <span order_id="{{$order->id}}" order="true" class="highlights" style="display:inline-block;">{{$order->price}} SAR</span>
                    <div order_id="{{$order->id}}" order="true" style="display:block" id="status_parent{{$order->id}}">
                        <div order_id="{{$order->id}}" order="true" id="status{{$order->id}}" class="status statusB"></div><span id="status_txt_{{$order->id}}">{{$order->status}}</span>
                    </div>
                </div>
            <script>current_preparing.push(parseInt({{$order->id}}))</script>
            @endforeach

        </div>
        <div id="ready" ondrop="drop(event)" ondragover="allowDrop(event)" name="ready" acceptDrop = "true">
            <p style="font-size:larger">Ready</p>
            @php
                $orders = App\Order::where('branch_id',$user)->where('status','ready')->get();
            @endphp

            @foreach($orders as $order)
                <div   draggable="true" ondragstart="drag(event)" id="drag{{1024+$order->id}}" class="movable movgreen" order_id="{{$order->id}}" order="true" status="ready"> 
                    <span order_id="{{$order->id}}" order="true" class="highlights" style="display:block;">{{$order->id}}</span>
                    Order Summary <span order_id="{{$order->id}}" order="true" class="highlights" style="display:inline-block;">{{$order->price}} SAR</span>
                    <div order_id="{{$order->id}}" order="true" style="display:block" id="status_parent{{$order->id}}">
                        <div order_id="{{$order->id}}" order="true" id="status{{$order->id}}" class="status statusG"></div> <span id="status_txt_{{$order->id}}">{{$order->status}}</span>
                    </div>
                </div>
                <script>current_ready.push(parseInt({{$order->id}}))</script>
            @endforeach
        </div>

        <div class="result" id="result" result="true">
        </div>

        <script src="{{ asset('js/main.js') }}"></script>
        <script>
            window.onclick = function(event) {
                if(event.target.getAttribute("order") == null && event.target.getAttribute("result") == null){
                document.getElementById("result").innerHTML ="";
                return;
                }
                if(event.target.getAttribute("result") == 'true'){
                return;
                }
                callOrderGraphQL(event.target.getAttribute("order_id"),document.getElementById("result"));
            }

            function callOrderGraphQL(id, sidePanel)
            {
                $.ajax({
                    type: 'POST', 
                    url: '/graphql',
                    data:{query:'query{order(id:'+id+'){orderItems{count,foodItem{restaurant_code,price},itemSize{price,name},addons{price,name,code}} notes }}'},
                    success: function (data) {
                        var order = data.data.order;
                        myHTML = '<div result="true" style="text-align:left; border-bottom:1px solid black;"><p result="true" style="font-size:larger; margin-bottom:10px;">Order Number: '+ (parseInt(id, 10)) +'</p></div>';
                        myHTML +='<table style="width:100%"> <tr> <th>QTY</th> <th>ITEM</th> <th>Price</th> </tr>';
                        
                        orderItems = order.orderItems;
                        for (i = 0; i < orderItems.length; i++) {
                            orderItem = orderItems[i];

                            addonHtml = "";
                            addons = orderItem.addons;
                            for (j = 0; j < addons.length; j++) {
                                addon = addons[j];
                                
                                addonHtml +=`<span style="color:#7777FF"> ${addon.code} </span>`;
                                if(j+1 >= addons.length){
                                    addonHtml =": "+addonHtml+".";
                                    continue;
                                }
                                addonHtml += ", ";
                            }

                            myHTML += `<tr><td>${orderItem.count}</td><td>${orderItem.foodItem.restaurant_code + addonHtml}</td>`;
                            if(orderItem.itemSize !== 'undefined' && orderItem.itemSize != null){
                                myHTML += `<td>${orderItem.itemSize.price}</td></tr>`;
                            }else{
                                myHTML += `<td>${orderItem.foodItem.price}</td></tr>`;
                            }

                        } 
                        myHTML += '</table>';

                        notes = "";
                        if(order.notes != null){
                            notes = order.notes;
                        }
                        myHTML += '<div result="true" style="margin-top:10px; text-align:left; border-top:1px solid black;"><p result="true" style="margin-top:10px;"><span style="color:#7777FF">Notes:</span> <span style="font-size:smaller;">'+ notes +'</span></p></div>';
                        document.getElementById("result").innerHTML = myHTML;
                    },
                    error: function() { 
                        console.log(data);
                    }
                });
            };
        </script>

        <script src="https://www.gstatic.com/firebasejs/8.2.2/firebase-app.js"></script>
        <script src="https://www.gstatic.com/firebasejs/8.2.2/firebase-database.js"></script>
        <script>
            var config = {
                apiKey: "{{env('FBAPIKEY')}}",
                authDomain: "{{env('GOOGLE_CLOUD_PROJECT')}}.firebaseapp.com",
                databaseURL: " https://{{env('GOOGLE_CLOUD_PROJECT')}}.firebaseio.com/",
                storageBucket: "{{env('GOOGLE_CLOUD_PROJECT')}}.appspot.com"
            };
            firebase.initializeApp(config);
            var database = firebase.database();

            database.ref('/orders').on('value', (snapshot) => {
                const data = snapshot.val();
                for (const [key, value] of Object.entries(data)) {
                    console.log(key);
                    if(value["meta-data"] === undefined){
                        continue;
                    }
                    else if(value["meta-data"].branch_id === undefined  || parseInt(value["meta-data"].branch_id) != parseInt({{$user}})){
                        console.log("Current Branch: "+ parseInt({{$user}}));
                        console.log("Incoming Branch: "+ parseInt(value["meta-data"].branch_id));
                        continue;
                    }

                    else if(value["meta-data"].status == "waiting_branch"){
                        if(current_waiting.includes(parseInt(value["meta-data"].order_id))){
                            continue;
                        }
                        getOrderPrice(value["meta-data"].order_id, 'waiting_branch', current_waiting);
                    }

                    else if(value["meta-data"].status == "preparing"){
                        if(current_preparing.includes(parseInt(value["meta-data"].order_id))){
                            continue;
                        }
                        removeA(current_waiting, parseInt(value["meta-data"].order_id));
                        var_id = value["meta-data"].order_id+1024;
                        document.getElementById("drag"+var_id).remove();

                        getOrderPrice(value["meta-data"].order_id, 'preparing', current_preparing);
                    }

                    else if(value["meta-data"].status == "ready"){
                        if(current_ready.includes(parseInt(value["meta-data"].order_id))){
                            continue;
                        }
                        removeA(current_preparing, parseInt(value["meta-data"].order_id));
                        var_id = value["meta-data"].order_id+1024;
                        document.getElementById("drag"+var_id).remove();

                        getOrderPrice(value["meta-data"].order_id, 'ready', current_ready);
                    }

                    else if(value["meta-data"].status == "on_his_way"){
                        if(!current_ready.includes(parseInt(value["meta-data"].order_id))){
                            continue;
                        }
                        removeA(current_ready, parseInt(value["meta-data"].order_id));
                        var_id = value["meta-data"].order_id+1024;
                        document.getElementById("drag"+var_id).remove();
                    }
                }
            });
        </script>

        <script>
            function addOrder(id, price, parent, parent_array, customer){
                switch(parent){
                    case"waiting_branch": color = "red"; letter="R"; break;
                    case"preparing": color = "blue"; letter="B"; break;
                    case"ready": color = "green"; letter="G"; break;
                }
                parent_array.push(parseInt(id));
                ktf = 1024+id;
                tk = id;
                document.getElementById(parent).innerHTML += 
                `
                <div draggable="true" ondragstart="drag(event)"id="drag`+ktf+`" class="movable mov`+color+`" order_id="`+id+`" order="true" status="`+parent+`"> 
                    <span order_id="`+id+`" order="true" class="highlights" style="display:block;">`+tk+`</span>
                        Order Summary <span order_id="`+id+`" order="true" class="highlights" style="display:inline-block;"> `+price+` SAR</span>
                    <div order_id="`+id+`" order="true" style="display:block" id="status_parent`+id+`">
                    <div order_id="`+id+`" order="true" id="status`+id+`" class="status status`+letter+`"></div><span id="status_txt_`+id+`">`+parent+`</span>
                    </div>
                </div>
                `;
            }

            function getOrderPrice(id, parent, parent_array)
            {
                $.ajax({
                    type: 'POST', 
                    url: '/graphql',
                    data:{query:'mutation{updateOrder(id:'+id+'){id price customer{username address{description}} }}'},
                    success: function (data) {
                        res = data.data.updateOrder;
                        addOrder(id, res.price, parent, parent_array, res.customer)
                    },
                    error: function() { 
                        console.log(data);
                    }
                });
            };

            // hide analytics
            window.addEventListener('load', (event) => {
                var analytics =document.getElementsByClassName('analytics-container')[0];
                analytics.style.display="none";
            });
        </script>

    </body>
</html>