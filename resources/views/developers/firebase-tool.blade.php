<div>
    <!-- Order Section -->
    <h3>Go To Order</h3>
        Enter Order ID
    <input type="number" wire:model="order_id">
    <button wire:click="goToOrder">Go</button>
    @if($order_result) 
        <p> {{$order_result}} </p>
    @elseif($order_error) 
        <p class="firebase-error"> Couldn't Find Order </p>
    @endif

    <!-- Driver Section -->
    <h3>Go To Driver</h3>
        Enter Driver ID
    <input type="number" wire:model="driver_id">
    <button wire:click="goToDriver">Go</button>
    @if($driver_result) 
        <p> {{$driver_result}} </p>
    @elseif($driver_error) 
        <p class="firebase-error"> Couldn't Find Driver </p>
    @endif

    <script>
        Livewire.on('goToLink', input => {
            window.location.replace(input);
        })
    </script>

</div>
