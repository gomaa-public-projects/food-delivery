<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Developers</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            .firebase-error{
                color:red;
            }   
        </style>
    </head>
    <body>
        @livewireScripts
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div id="firebase_tool">
                    @livewire('firebase-tool')
                </div>
            </div>
        </div>
    </body>
</html>
