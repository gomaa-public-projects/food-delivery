
 function getLoading(){
    return $('#loading-container').html();
 }

$('.btn_create').on('click',(e)=>{
    current_model_slug = $(e.currentTarget)[0].getAttribute('data-create');
    current_model_slug =current_model_slug .replace('_', '-');
    if(family_tree[current_model_slug] === undefined){
        console.log("slug: "+current_model_slug+" is not in family tree: "+family_tree);
    }
    parent = family_tree[current_model_slug].parent;
    parentId = family_tree[current_model_slug].parentId;
    model_name = family_tree[current_model_slug].model_name;
    parent_slug = family_tree[current_model_slug].parent_slug;
    ajax=""
    if(family_tree[current_model_slug].ajax){
        ajax=`${parent_slug}-`;
    }
    $(`#${ajax}create-edit-modal-body`).html(getLoading());
    $(`#${ajax}create-edit-modal #${ajax}title`).text($(e.target).text());
    $(`#${ajax}create-edit-modal #${ajax}close_button`)[0].setAttribute("name", model_name+"-close");
    $.ajax({
        url: '/admin/'+$(e.target).data("create").replace('_', '-')+`/create?parent=${parent}&parentId=${parentId}`,
        type: 'GET',
        success: function(data){
            $(`#${ajax}create-edit-modal-body`).html(data);
            //console.log(current_model_slug ,parent ,parentId ,model_name);
            console.log($(e.target).text());
        },
    })
});

$('.btn_edit').on('click',(e)=>{
    current_model_slug = $(e.currentTarget)[0].getAttribute('data-slug');
    current_model_slug =current_model_slug .replace('_', '-');
    if(family_tree[current_model_slug] === undefined){
        console.log("slug: "+current_model_slug+" is not in family tree: "+family_tree);
    }
    parent = family_tree[current_model_slug].parent;
    parentId = family_tree[current_model_slug].parentId;
    model_name = family_tree[current_model_slug].model_name;
    parent_slug = family_tree[current_model_slug].parent_slug;
    ajax=""
    if(family_tree[current_model_slug].ajax){
        ajax=`${parent_slug}-`;
    }

    $(`#${ajax}create-edit-modal-body`).html($(e.target).data("edit"));
    $(`#${ajax}create-edit-modal #${ajax}title`).text("Edit "+$(e.currentTarget)[0].getAttribute('data-slug').replace('-', ' '));
    $(`#${ajax}create-edit-modal #${ajax}close_button`)[0].setAttribute("name", model_name+"-close");
    $.ajax({
        url: '/admin/'+$(e.target).data("slug").replace('_', '-')+'/'+$(e.target).data("edit")+`/edit?parent=${parent}&parentId=${parentId}`,
        type: 'GET',
        success: function(data){
            console.log("edit "+$(e.currentTarget)[0].getAttribute('data-slug'));
            $(`#${ajax}create-edit-modal-body`).html(data);
        },
    })
});


$('.btn_delete').on('click',(i)=>{
    $("#myID").val($(i.target).data("delete"));
    temp = $(i.target).data("slug"); 

});

$('#del-btn').on('click',function(e){
      e.preventDefault();

      var deleted = $("#myID").val();
       $.ajax({
        url: '/admin/'+temp+'/'+deleted,
        type: 'DELETE',
        success: function(){
          location.reload();
        },
        error: function(xhr, desc, err) {
          location.reload();
        },
      })
      
    });