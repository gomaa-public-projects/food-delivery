(function($) {
    /* "use strict" */


 var dzChartlist = function(){
	
	var screenWidth = $(window).width();
		
	var widgetChart2 = function(){
		if(jQuery('#widgetChart2').length > 0 ){
			const chart_widget_2 = document.getElementById("widgetChart2").getContext('2d');

			$.ajax({
				type: 'POST', 
				url: '/graphql',
				data:{query:`query{totalOrders{percentage lables values total}}`},
				success: function (data) {
					console.log(data);
					var totalOrders = data.data.totalOrders;
					console.log(totalOrders.percentage);
					$('#orders_count').html(totalOrders.total + " "+totalOrders.percentage);
					var lables_names = JSON.parse(totalOrders.lables);
					var values = JSON.parse(totalOrders.values);
					new Chart(chart_widget_2, {
						type: "line",
						data: {
							labels: lables_names,
							datasets: [{
								label: "Orders",
								backgroundColor: ['rgba(34, 167, 240, .13)'],
								borderColor: '#22a7f0',
								pointBackgroundColor: '#22a7f0',
								pointBorderColor: '#22a7f0',
								borderWidth:2,
								pointHoverBackgroundColor: '#22a7f0',
								pointHoverBorderColor: '#22a7f0',
								data: values
							}]
						},
						options: {
							title: {
								display: !1
							},
							tooltips: {
								intersect: !1,
								mode: "nearest",
								xPadding: 10,
								yPadding: 10,
								caretPadding: 10
							},
							legend: {
								display: !1
							},
							responsive: !0,
							maintainAspectRatio: !1,
							hover: {
								mode: "index"
							},
							scales: {
								xAxes: [{
									display: !1,
									gridLines: !1,
									scaleLabel: {
										display: !0,
										labelString: "Month"
									}
								}],
								yAxes: [{
									display: !1,
									gridLines: !1,
									scaleLabel: {
										display: !0,
										labelString: "Value"
									},
									ticks: {
										beginAtZero: !0
									}
								}]
							},
							elements: {
								line: {
									tension: .15
								},
								point: {
									radius: 0,
									borderWidth: 0
								}
							},
							layout: {
								padding: {
									left: 0,
									right: 0,
									top: 5,
									bottom: 0
								}
							}
						}
					});

				},
				error: function() { 
					console.log(data);
				}
			});

			

		}
	}
	var widgetChart3 = function(){
		if(jQuery('#widgetChart3').length > 0 ){
			const chart_widget_3 = document.getElementById("widgetChart3").getContext('2d');


			$.ajax({
				type: 'POST', 
				url: '/graphql',
				data:{query:`query{totalCustomers{percentage lables values total}}`},
				success: function (data) {
					console.log(data);
					var totalCustomers = data.data.totalCustomers;
					console.log(totalCustomers.percentage);
					$('#customers_total').html(parseInt(totalCustomers.total) + " "+totalCustomers.percentage);
					var lables_names = JSON.parse(totalCustomers.lables);
					var values = JSON.parse(totalCustomers.values);

					new Chart(chart_widget_3, {
						type: "line",
						data: {
							labels: lables_names,
							datasets: [{
								label: "Customers",
								backgroundColor: ['rgba(34, 167, 240, .13)'],
								borderColor: '#22a7f0',
								pointBackgroundColor: '#22a7f0',
								pointBorderColor: '#22a7f0',
								borderWidth:2,
								pointHoverBackgroundColor: '#22a7f0',
								pointHoverBorderColor: '#22a7f0',
								data: values
							}]
						},
						options: {
							title: {
								display: !1
							},
							tooltips: {
								intersect: !1,
								mode: "nearest",
								xPadding: 10,
								yPadding: 10,
								caretPadding: 10
							},
							legend: {
								display: !1
							},
							responsive: !0,
							maintainAspectRatio: !1,
							hover: {
								mode: "index"
							},
							scales: {
								xAxes: [{
									display: !1,
									gridLines: !1,
									scaleLabel: {
										display: !0,
										labelString: "Month"
									}
								}],
								yAxes: [{
									display: !1,
									gridLines: !1,
									scaleLabel: {
										display: !0,
										labelString: "Value"
									},
									ticks: {
										beginAtZero: !0
									}
								}]
							},
							elements: {
								line: {
									tension: .15
								},
								point: {
									radius: 0,
									borderWidth: 0
								}
							},
							layout: {
								padding: {
									left: 0,
									right: 0,
									top: 5,
									bottom: 0
								}
							}
						}
					});
				},
				error: function() { 
					console.log(data);
				}
			});

		}
	}
	var widgetChart4 = function(){
		if(jQuery('#widgetChart4').length > 0 ){
			const chart_widget_4 = document.getElementById("widgetChart4").getContext('2d');

			$.ajax({
				type: 'POST', 
				url: '/graphql',
				data:{query:`query{totalRevenue{percentage lables values total}}`},
				success: function (data) {
					console.log(data);
					var totalRevenue = data.data.totalRevenue;
					$('#revenue_total').html(totalRevenue.total + " "+totalRevenue.percentage);
					var lables_names = JSON.parse(totalRevenue.lables);
					var values = JSON.parse(totalRevenue.values);

					new Chart(chart_widget_4, {
						type: "line",
						data: {
							labels: lables_names,
							datasets: [{
								label: "Profits",
								backgroundColor: ['rgba(34, 167, 240, .13)'],
								borderColor: '#22a7f0',
								pointBackgroundColor: '#22a7f0',
								pointBorderColor: '#22a7f0',
								borderWidth:2,
								pointHoverBackgroundColor: '#22a7f0',
								pointHoverBorderColor: '#22a7f0',
								data: values
							}]
						},
						options: {
							title: {
								display: !1
							},
							tooltips: {
								intersect: !1,
								mode: "nearest",
								xPadding: 10,
								yPadding: 10,
								caretPadding: 10
							},
							legend: {
								display: !1
							},
							responsive: !0,
							maintainAspectRatio: !1,
							hover: {
								mode: "index"
							},
							scales: {
								xAxes: [{
									display: !1,
									gridLines: !1,
									scaleLabel: {
										display: !0,
										labelString: "Month"
									}
								}],
								yAxes: [{
									display: !1,
									gridLines: !1,
									scaleLabel: {
										display: !0,
										labelString: "Value"
									},
									ticks: {
										beginAtZero: !0
									}
								}]
							},
							elements: {
								line: {
									tension: .15
								},
								point: {
									radius: 0,
									borderWidth: 0
								}
							},
							layout: {
								padding: {
									left: 0,
									right: 0,
									top: 5,
									bottom: 0
								}
							}
						}
					});
					
				},
				error: function() { 
					console.log(data);
				}
			});
		}
	}

	var myChartBar = function(){

		var type = findType('select_revenue');

		$.ajax({
			type: 'POST', 
			url: '/graphql',
			data:{query:`query{total_`+type+`_revenue{lables values values_net total total_net}}`},
			success: function (data) {
				console.log(data);
				var totalRevenue = data.data[`total_`+type+`_revenue`];
				$('#net_orders_prices').html("$ "+totalRevenue.total_net);
				$('#total_orders_prices').html("$ "+totalRevenue.total);
				var lables_names = JSON.parse(totalRevenue.lables);
				var values_one = JSON.parse(totalRevenue.values);
				var values_two = JSON.parse(totalRevenue.values_net);
				chartBar(lables_names, values_one, values_two);
			},
			error: function() { 
				console.log(data);
			}
		});
	}

	var chartBar = function(lables, data_one, data_two){

		var options = {
			  series: [
				{
					name: 'Total Profits',
					data: data_one,
					//radius: 12,	
				}, 
				{
				  name: 'Profits Without Delivery',
				  data: data_two
				}, 
				
			],
				chart: {
				type: 'area',
				height: 350,
				toolbar: {
					show: false,
				},
				
			},
			plotOptions: {
			  bar: {
				horizontal: false,
				columnWidth: '55%',
				endingShape: 'rounded'
			  },
			},
			colors:['#bcd2dd', '#22A7F0'],
			dataLabels: {
			  enabled: false,
			},
			markers: {
		shape: "circle",
		},
		
		
			legend: {
				show: true,
				fontSize: '12px',
				
				labels: {
					colors: '#000000',
					
				},
				position: 'bottom',
				horizontalAlign: 'center', 	
				markers: {
					width: 19,
					height: 19,
					strokeWidth: 0,
					strokeColor: '#fff',
					fillColors: undefined,
					radius: 4,
					offsetX: 0,
					offsetY: 0
				}
			},
			stroke: {
			  show: true,
			  width: 0,
			  colors:['#bcd2dd', '#22A7F0'],
			},
			
			grid: {
				borderColor: '#eee',
			},
			xaxis: {
				
			  categories: lables,
			  labels: {
				style: {
					colors: '#3e4954',
					fontSize: '13px',
					fontFamily: 'Poppins',
					fontWeight: 100,
					cssClass: 'apexcharts-xaxis-label',
				},
			  },
			  crosshairs: {
			  show: false,
			  }
			},
			yaxis: {
				labels: {
			   style: {
				  colors: '#3e4954',
				  fontSize: '13px',
				   fontFamily: 'Poppins',
				  fontWeight: 100,
				  cssClass: 'apexcharts-xaxis-label',
			  },
			  },
			},
			fill: {
				type: 'solid',
				opacity: 0.8,
			},
			tooltip: {
			  y: {
				formatter: function (val) {
				  return "$ " + val
				}
			  }
			}
			};

			$('#chartBar').html("");
			var chartBar1 = new ApexCharts(document.querySelector("#chartBar"), options);
			chartBar1.render();
	}

	var myChartBar_2 = function(){

		var type = findType('select_delivery');

		$.ajax({
			type: 'POST', 
			url: '/graphql',
			data:{query:`query{total_`+type+`_delivery{lables_delivery values_delivery total_delivery}}`},
			success: function (data) {
				console.log(data);
				var totalDelivery = data.data[`total_`+type+`_delivery`];
				$('#total_delivery').html("$ "+totalDelivery.total_delivery);
				var lables_names = JSON.parse(totalDelivery.lables_delivery);
				var values = JSON.parse(totalDelivery.values_delivery);
				chartBar_2(lables_names, values);
			},
			error: function() { 
				console.log(data);
			}
		});
	}

	var chartBar_2 = function(lables, data_one){

		var options = {
			  series: [
				{
					name: 'Total Delivery',
					data: data_one,
					//radius: 12,	
				}, 
				
			],
				chart: {
				type: 'area',
				height: 350,
				toolbar: {
					show: false,
				},
				
			},
			plotOptions: {
			  bar: {
				horizontal: false,
				columnWidth: '55%',
				endingShape: 'rounded'
			  },
			},
			colors:['#bcd2dd', '#22A7F0'],
			dataLabels: {
			  enabled: false,
			},
			markers: {
		shape: "circle",
		},
		
		
			legend: {
				show: true,
				fontSize: '12px',
				
				labels: {
					colors: '#000000',
					
				},
				position: 'bottom',
				horizontalAlign: 'center', 	
				markers: {
					width: 19,
					height: 19,
					strokeWidth: 0,
					strokeColor: '#fff',
					fillColors: undefined,
					radius: 4,
					offsetX: 0,
					offsetY: 0
				}
			},
			stroke: {
			  show: true,
			  width: 0,
			  colors:['#bcd2dd', '#22A7F0'],
			},
			
			grid: {
				borderColor: '#eee',
			},
			xaxis: {
				
			  categories: lables,
			  labels: {
				style: {
					colors: '#3e4954',
					fontSize: '13px',
					fontFamily: 'Poppins',
					fontWeight: 100,
					cssClass: 'apexcharts-xaxis-label',
				},
			  },
			  crosshairs: {
			  show: false,
			  }
			},
			yaxis: {
				labels: {
			   style: {
				  colors: '#3e4954',
				  fontSize: '13px',
				   fontFamily: 'Poppins',
				  fontWeight: 100,
				  cssClass: 'apexcharts-xaxis-label',
			  },
			  },
			},
			fill: {
				type: 'solid',
				opacity: 0.8,
			},
			tooltip: {
			  y: {
				formatter: function (val) {
				  return "$ " + val
				}
			  }
			}
			};

			$('#chartBar_2').html("");
			var chartBar1 = new ApexCharts(document.querySelector("#chartBar_2"), options);
			chartBar1.render();
	}

	var findType = function(select){
		type = "";
		children = document.getElementById(select).getElementsByTagName('li');
		for (var i=0; i<3; i++){
			child = children[i];
			if(child.firstElementChild.classList.contains('active')){
				type = child.firstElementChild.getAttribute('name');
				break;
			}
		}
		return type;
	}

	var myLatest= function(){

		$.ajax({
			type: 'POST', 
			url: '/graphql',
			data:{query:`query{latestOrders{orders{id total_price status orderItems{foodItem{name image}} customer{username address{description}}}}}`},
			success: function (data) {
				console.log(data);
				var totalRevenue = data.data.latestOrders;
				var orders = totalRevenue.orders;
				var html = "";
				for (i = 0; i < orders.length; i++) {
					order = orders[i];

					console.log(order.orderItems.length);
					if(order.orderItems.length == 0){
						continue;
					}

					id = parseInt(order.id, 10);
					image = order.orderItems[0].foodItem.image;
					food_content = "";
					for (j = 0; j < order.orderItems.length && j < 3; j++) {
						food_content += order.orderItems[j].foodItem.name+",";						
					}
					food_content = food_content.slice(0, -1)
					if(order.orderItems.length> 3) food_content += ", ...";
					
					customer_name = order.customer.username;
					customer_address = order.customer.address.description;
					if(typeof customer_address === 'string' && customer_address.length> 50){
						customer_address = customer_address.substring(0, 50); 
					}
					customer_address += "..."

					switch(order.status){
						case"PENDING":status_class = "bgl-warning text-warning"; break;
						case"WAITING_DRIVER":status_class = "bgl-warning text-warning"; break;
						case"ACCEPTED":status_class = "bgl-warning text-warning"; break;
						case"WAITING_BRANCH":status_class = "bgl-warning text-warning"; break;
						case"PREPARING":status_class = "bgl-warning text-warning"; break;
						case"READY":status_class = "bgl-success text-success"; break;
						case"ON_HIS_WAY":status_class = "bgl-success text-success"; break;
						case"DELIVERED":status_class = "bgl-success text-success"; break;
						case"REJECTED":status_class = "bgl-light"; break;
						case"ARCHIVED":status_class = "bgl-light"; break;
						case"CLOSED":status_class = "bgl-light"; break;
						default:status_class = "bgl-warning text-warning"; break;

					}

					html+=`
					<tr>
						<td>
							<div class="media align-items-center">
								<img class="mr-3 img-fluid rounded-circle" width="75" src="${image}" alt="DexignZone">
								<div class="media-body">
									<h5 class="mt-0 mb-2"><a class="text-black">${food_content}</a></h5>
									<p class="mb-0" style="color:#22a7f0;">#${id}</p>
								</div>
							</div>
						</td>
						<td>
							<h5 class="mb-2 text-black wspace-no">${customer_name}</h5>
							<p class="mb-0">${customer_address}</p>
						</td>
						<td style="width:100px;">
							<div class="d-flex align-items-center justify-content-center">
								<h4 class="mb-0 mr-3 fs-20 text-black d-inline-block">$${order.total_price}</h4>
							</div>
						</td>
						<td>
							<div class="d-flex align-items-center">
								<a class="btn ${status_class}" href="javascript:;">${order.status}</a>
							</div>
						</td>
					</tr>	
					`;
				}
				
				$('#timeline_orders').html(html);
			},
			error: function() { 
				console.log(data);
			}
		});
	}

	var myTop= function(){

		$.ajax({
			type: 'POST', 
			url: '/graphql',
			data:{query:`query{ DashboardPopularFood{ foodItem{ name image price } count rank } }`},
			success: function (data) {
				console.log(data);
				var popularFood = data.data.DashboardPopularFood;
				var html = "";
				for (i = 0; i < popularFood.length; i++) {
					list_item = popularFood[i];
					rank = list_item.rank;
					image = list_item.foodItem.image;
					count = list_item.count;
					price = list_item.foodItem.price;
					item_name =list_item.foodItem.name;

					html+=`
					<li>
						<div class="timeline-panel">
							<div class="media mr-3">
								<img alt="image" width="90"  src="${image}">
								<div class="number">#${rank}</div>
							</div>
							<div class="media-body">
								<h5 class="mb-3"><a class="text-black">${item_name}</a></h5>
								<div class="d-flex justify-content-between align-items-center">
									<h4 class="mb-0 text-black font-w600">$${price}</h4>
									<p class="mb-0">Order <strong class="text-black font-w500">${count}x</strong></p>
								</div>
							</div>
						</div>
					</li>
					`;
				}
				
				$('#food_timeline').html(html);
			},
			error: function() { 
				console.log(data);
			}
		});
	}
	
	/* Function ============ */
		return {
			init:function(){
				
			},
			
			
			load:function(){
				widgetChart2();	
				widgetChart3();	
				widgetChart4();	
				myChartBar();
				myChartBar_2();
				myLatest();	
				myTop();
			},
			
			resize:function(){
			},

			loadChartOne:function(){	
				myChartBar();
			},

			loadChartTwo:function(){
				myChartBar_2();	
			},
		}
	
	}();

	jQuery(document).ready(function(){
	});
		
	jQuery(window).on('load',function(){
		setTimeout(function(){
			dzChartlist.load();
		}, 1000); 
		
	});

	jQuery(window).on('resize',function(){
		
		
	});   
	
	window.onclick = function(event) {
		if(event.target.hasAttribute('select_revenue')){
			dzChartlist.loadChartOne();
		}
		if(event.target.hasAttribute('select_delivery')){
			dzChartlist.loadChartTwo();
		}
		if(event.target.hasAttribute('visit_dashboard')){
			window.location.replace('dashboardCustom');
		}
		if(event.target.hasAttribute('visit_orders')){
			win = window.open('admin/orders', '_blank');
  			win.focus();
		}
		if(event.target.hasAttribute('visit_customers')){
			win = window.open('admin/customers', '_blank');
  			win.focus();
		}
	}

})(jQuery);