$('#datepicker_from').datepicker({
    format: 'yyyy-mm-dd',
    useCurrent: false
}).datepicker("setDate", new Date());
$('#datepicker_to').datepicker({
    format: 'yyyy-mm-dd',
    useCurrent: false
}).datepicker("setDate", new Date());

$("#branches_check").change(function() {
    callOrdersTotalGraphQL();
    if(this.checked) {
         $('#branches').prop("disabled", true);
         return;
    }
    $('#branches').removeAttr('disabled');
});

$('input[type=radio][name=period]').change(function() {
    $('#radio').val(this.value);
    callOrdersTotalGraphQL();
    if(this.value != 'custom') {
         $('#datepicker_from').prop("disabled", true);
         $('#datepicker_to').prop("disabled", true);
         return;
    }
    $('#datepicker_from').removeAttr('disabled');
    $('#datepicker_to').removeAttr('disabled');
});

$('#branches').focus(function(){
    callBranchGraphQL();
});

$(window).load(function() {
    $("#custom").prop("checked", true);
    callOrdersTotalGraphQL();
});
$('#branches').change(function(){
    callOrdersTotalGraphQL();
});
$('#datepicker_from').change(function(){
    callOrdersTotalGraphQL();
});
$('#datepicker_to').change(function(){
    callOrdersTotalGraphQL();
});


function callBranchGraphQL()
{
    $.ajax({
        type: 'POST', 
        url: '/graphql',
        data:{query:`query{branches{id,name}}`},
        success: function (data) {
            console.log(data);
            var branches = data.data.branches;
            $('#branches').html("");
            html="";
            for (i = 0; i < branches.length; i++) {
                branch = branches[i];
                html+=`<option value=${branch.id}>${branch.name}</option>`;
            }
            $('#branches').html(html);
        },
        error: function() { 
            console.log(data);
        }
    });
};

function callOrdersTotalGraphQL()
{
    branches = $('#branches').val();
    if(branches == null){
        return;
    }
    datepicker_from = $('#datepicker_from').val();
    datepicker_to = $('#datepicker_to').val();

    open = "";
    closure = "";

    datepicker_from_q = "";
    datepicker_to_q = "";
    if(datepicker_from != ""){
        open = "(";
        closure=")";
        datepicker_from_q = `from:"${datepicker_from} 00:00:00"`; 
    }
    if(datepicker_to != ""){
        open = "(";
        closure=")";
        datepicker_to_q = `to:"${datepicker_to} 00:00:00"`;
    }
    
    
    branches_q = "";

    if(!$('#branches_check').is(":checked")){
        open = "(";
        closure=")";
        branches_q = `branch_id:${branches}`;
    }

    today = "";

    if($('#radio').val() != 'custom'){
        open = "(";
        closure=")";
        today = $('#radio').val() + ":true";
    }

    $.ajax({
        type: 'POST', 
        url: '/graphql',
        data:{query:`query{orders${open} ${branches_q} ${datepicker_from_q} ${datepicker_to_q} ${today} ${closure}{count total orders{ id total_price status delivery}}}`},
        success: function (data) {
            console.log(data);
            var total_orders = data.data.orders;
            
            $('#total_orders').html(total_orders.count);
            $('#total_profits').html(parseFloat(total_orders.total).toFixed(2));

            var html = "";
            var myOrders = total_orders.orders;

            for (i = 0; i < myOrders.length; i++) {
                myOrder = myOrders[i];

                switch(myOrder.status){
                    case"PENDING":
                    case"WAITING_DRIVER":
                    case"ACCEPTED":
                    case"WAITING_BRANCH":
                        color = "red"; 
                        letter = "R";
                        break;
                    case"PREPARING":
                        color = "blue"; 
                        letter = "B";
                        break;

                    default:
                        color = "green"; 
                        letter = "G";
                        break;
                }
                

                ktf = 1024 + myOrder.id;
                tk = 10000 + myOrder.id;

                price = myOrder.total_price - myOrder.delivery;
                price = parseFloat(price).toFixed(2);
                id = myOrder.id;

                html += `
                <div id="drag`+ktf+`" class="movable mov`+color+` col-xs-6 col-sm-4 col-md-2" order_id="`+id+`" order="true" status="`+myOrder.status+`"> 
                    <span order_id="`+id+`" order="true" class="highlights" style="display:block;">`+tk+`</span>
                        Order Summary <span order_id="`+id+`" order="true" class="highlights" style="display:inline-block;"> `+price+` SAR</span>
                    <div order_id="`+id+`" order="true" style="display:block" id="status_parent`+id+`">
                    <div order_id="`+id+`" order="true" id="status`+id+`" class="status status`+letter+`"></div><span id="status_txt_`+id+`">`+myOrder.status+`</span>
                    </div>
                </div>
                `;
                
            } 
            $('#main_mini_orders').html(html);


        },
        error: function() { 
            console.log(data);
        }
    });
};