

$('#datepicker_from_driver').datepicker({
    format: 'yyyy-mm-dd',
    useCurrent: false
}).datepicker("setDate", new Date());
$('#datepicker_to_driver').datepicker({
    format: 'yyyy-mm-dd',
    useCurrent: false
}).datepicker("setDate", new Date());

$("#drivers_check").change(function() {
    callDriversTotalGraphQl();
    if(this.checked) {
         $('#drivers').prop("disabled", true);
         return;
    }
    $('#drivers').removeAttr('disabled');
});

$('input[type=radio][name=period_driver]').change(function() {
    $('#radio_driver').val(this.value);
    callDriversTotalGraphQl();
    if(this.value != 'custom') {
         $('#datepicker_from_driver').prop("disabled", true);
         $('#datepicker_to_driver').prop("disabled", true);
         return;
    }
    $('#datepicker_from_driver').removeAttr('disabled');
    $('#datepicker_to_driver').removeAttr('disabled');
});

$('#drivers').focus(function(){
    callBranchGraphQL();
});

$(window).load(function() {
    $("#custom_driver").prop("checked", true);
    callDriversTotalGraphQl();
});
$('#drivers').change(function(){
    callDriversTotalGraphQl();
});
$('#datepicker_from_driver').change(function(){
    callDriversTotalGraphQl();
});
$('#datepicker_to_driver').change(function(){
    callDriversTotalGraphQl();
});


function callBranchGraphQL()
{
    $.ajax({
        type: 'POST', 
        url: '/graphql',
        data:{query:`query{drivers{id,name}}`},
        success: function (data) {
            console.log(data);
            var drivers = data.data.drivers;
            $('#drivers').html("");
            html="";
            for (i = 0; i < drivers.length; i++) {
                branch = drivers[i];
                html+=`<option value=${branch.id}>#${branch.id}: ${branch.name}</option>`;
            }
            $('#drivers').html(html);
        },
        error: function() { 
            console.log(data);
        }
    });
};

function callDriversTotalGraphQl()
{
    drivers = $('#drivers').val();
    if(drivers == null){
        return;
    }
    datepicker_from = $('#datepicker_from_driver').val();
    datepicker_to = $('#datepicker_to_driver').val();

    open = "";
    closure = "";

    datepicker_from_q = "";
    datepicker_to_q = "";
    if(datepicker_from != ""){
        open = "(";
        closure=")";
        datepicker_from_q = `from:"${datepicker_from} 00:00:00"`; 
    }
    if(datepicker_to != ""){
        open = "(";
        closure=")";
        datepicker_to_q = `to:"${datepicker_to} 00:00:00"`;
    }
    
    
    branches_q = "";

    if(!$('#drivers_check').is(":checked")){
        open = "(";
        closure=")";
        branches_q = `driver_id:${drivers}`;
    }

    today = "";

    if($('#radio_driver').val() != 'custom'){
        open = "(";
        closure=")";
        today = $('#radio_driver').val() + ":true";
    }

    $.ajax({
        type: 'POST', 
        url: '/graphql',
        data:{query:`query{orders_driver${open} ${branches_q} ${datepicker_from_q} ${datepicker_to_q} ${today} ${closure}{count total}}`},
        success: function (data) {
            console.log(data);
            var total_orders = data.data.orders_driver;
            
            $('#total_orders_driver').html(total_orders.count);
            $('#total_profits_driver').html(parseFloat(total_orders.total).toFixed(2));
        },
        error: function() { 
            console.log(data);
        }
    });
};