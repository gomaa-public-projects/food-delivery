function allowDrop(ev) {
    ev.preventDefault();
  }
  
function drag(ev) {
    ev.dataTransfer.clearData();
    ev.dataTransfer.setData("text", ev.target.id);
  }
  
function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    console.log("Moving: "+document.getElementById(data).getAttribute("status"));
    console.log("Target: "+ev.target.getAttribute("acceptDrop"));
    if(ev.target.getAttribute("acceptDrop") == null || ev.target.getAttribute("name") == "new" || (ev.target.getAttribute("name") == "preparing" && document.getElementById(data).getAttribute("status") == "ready")){
        return;
    }

    if(ev.target.getAttribute("name") == "preparing"){
      id_var = document.getElementById(data).getAttribute("order_id");
      current_preparing.push(parseInt(id_var));
      updateOrderGraphQL(document.getElementById(data).getAttribute("order_id"), "PREPARING");
      document.getElementById(data).setAttribute("status","preparing");
      document.getElementById('status_txt_'+id_var).innerHTML = "preparing";
      document.getElementById(data).style["background-color"]  = "#EEEEFF";
      findChild(document.getElementById(data),"#0000FF",document.getElementById(data).getAttribute("order_id"));
    }
    if(ev.target.getAttribute("name") == "ready"){
      id_var = document.getElementById(data).getAttribute("order_id");
      current_ready.push(parseInt(id_var));
      updateOrderGraphQL(document.getElementById(data).getAttribute("order_id"), "READY");
      document.getElementById(data).setAttribute("status","ready");
      document.getElementById('status_txt_'+id_var).innerHTML = "ready";
      document.getElementById(data).style["background-color"]  = "#EEFFEE";
      findChild(document.getElementById(data),"#00FF00",document.getElementById(data).getAttribute("order_id"));
    }
    ev.target.appendChild(document.getElementById(data));
  }

// hide analytics
window.addEventListener('load', (event) => {
  var analytics =document.getElementsByClassName('analytics-container')[0];
  analytics.style.display="none";
  analytics =document.getElementsByClassName('site-footer-right')[0];
  analytics.style.display="none";
});

function findChild (parent, color,id){
  child = parent.querySelector("#status_parent"+id);
  child.querySelector("#status"+id).style["background-color"]  = color;
}

function updateOrderGraphQL(id, status)
  {
      $.ajax({
          type: 'POST', 
          url: '/graphql',
          data:{query:'mutation{updateOrder(id:'+id+' status:'+status+'){id}}'},
          success: function (data) {
              console.log(data);
          },
          error: function() { 
              console.log(data);
          }
      });
};

function removeA(arr) {
  var what, a = arguments, L = a.length, ax;
  while (L > 1 && arr.length) {
      what = a[--L];
      while ((ax= arr.indexOf(what)) !== -1) {
          arr.splice(ax, 1);
      }
  }
  return arr;
}

// System Functions

Element.prototype.remove = function() {
  this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
  for(var i = this.length - 1; i >= 0; i--) {
      if(this[i] && this[i].parentElement) {
          this[i].parentElement.removeChild(this[i]);
      }
  }
}
